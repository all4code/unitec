package Q1;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class NSLookup {

	public static void main(String[] args) {
		
		InetAddress[] inetAddress = null;
		String ip = null;

		if (args == null || args.length <= 0) {

			System.out.println("Please provide a Host name to lookup!");

		} else {

			try {

				inetAddress = InetAddress.getAllByName(args[0]);
				System.out.println("Host name: " + args[0]);

				if (inetAddress.length > 1) {
					for (int i = 0; i < inetAddress.length; i++) {
						ip = inetAddress[i].getHostAddress();
						printIPAddress(ip, i + 1);
					}
				} else {
					ip = inetAddress[0].getHostAddress();
					printIPAddress(ip,0);
				}

			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				System.out.println("The parameter that provided is not correcct!");
			}
		}
	}

	private static void printIPAddress(String ip, int num) {
		if (ip == null || ip.equals("")) {
			System.out.println("The IP address for the URL cannot be looked up!");
		} else {
			if (num == 0) {
				System.out.println("IP address: " + ip);
			} else {
				System.out.println("IP address " + num + " is: " + ip);
			}
		}
	}

}
