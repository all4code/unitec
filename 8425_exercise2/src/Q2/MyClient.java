package Q2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;

public class MyClient {
	Socket client;
	InputStream in;
	OutputStream out;
	SimpleDateFormat sdf;
	
	public MyClient() {
		try {
			sdf = new SimpleDateFormat("HH:mm:ss:S");
			client = new Socket("localhost",5000);
			System.out.println("[" + sdf.format(System.currentTimeMillis()) + "] Client start!");
			
			in = client.getInputStream();
			out = client.getOutputStream();
			
			out.write("Hello".getBytes());
			out.flush();
			client.shutdownOutput();
			System.out.println("[" + sdf.format(System.currentTimeMillis()) + "] Client sent: \"Hello\"");
			
			StringBuilder message = new StringBuilder();
			int c = 0;
			
			while (true) {
				while ((c = in.read()) != -1) {
					message.append((char) c);
				}
				
				if (message.toString().trim().equalsIgnoreCase("Hi there, got your message!")) {
					System.out.println("[" +  sdf.format(System.currentTimeMillis())  + "] Receive from server: \"" + message + "\"");
					break;
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			close();
		}
	}

	private void close() {
		System.out.println("[" +  sdf.format(System.currentTimeMillis())  + "] Client close!");
		try {
			if(out != null){
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			if(in != null){
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			if(client != null){
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		new MyClient();
	}
	
	
}
