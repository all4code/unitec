package Q2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.SimpleDateFormat;

public class MyServer {
	ServerSocket server;
	Socket client;
	OutputStream out;
	InputStream in;
	SimpleDateFormat sdf = null;

	public MyServer() {

		try {
			sdf = new SimpleDateFormat("HH:mm:ss:S");
			server = new ServerSocket(5000);
			System.out.println("[" +  sdf.format(System.currentTimeMillis())  + "] Server start!");

			client = server.accept();
			out = client.getOutputStream();
			in = client.getInputStream();

			StringBuilder message = new StringBuilder();
			int c = 0;
			String reply = "Hi there, got your message!";
			byte[] bytes = reply.getBytes();
			while (true) {
				
				while ((c = in.read()) != -1) {
					message.append((char) c);
				}

				System.out.println("[" + sdf.format(System.currentTimeMillis())  + "] Receive from MyClient: \"" + message + "\"");

				if (message.toString().trim().equalsIgnoreCase("hello")) {
					out.write(bytes, 0, bytes.length);
					out.flush();
					client.shutdownOutput();
					System.out.println("[" + sdf.format(System.currentTimeMillis())  + "] Server sent: \"" + reply + "\"");
					break;
				}
			}

		} catch (SocketException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			close();
		}
	}

	private void close() {
		System.out.println("[" +  sdf.format(System.currentTimeMillis())  + "] Server close!");
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (in != null) {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (client != null) {
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (server != null) {
				server.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new MyServer();
	}
}
