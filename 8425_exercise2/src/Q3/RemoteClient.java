package Q3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;

public class RemoteClient {
	Socket client;
	DataInputStream in;
	DataOutputStream out;
	
	public RemoteClient() {
		try {
			client = new Socket("localhost",5000);
			in = new DataInputStream(client.getInputStream());
			out = new DataOutputStream(client.getOutputStream());
			out.writeUTF("Hello");
			out.flush();
			long message = 0L;
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			while(true){
				message = in.readLong();
				if(message != 0L){
					System.out.println("The server’s local time is: " + df.format(message));
					break;
				}
			}
			close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void close() {
		System.out.println("Client close!");
		try {
			if(out != null){
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			if(in != null){
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			if(client != null){
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		new RemoteClient();
	}
	
	
}
