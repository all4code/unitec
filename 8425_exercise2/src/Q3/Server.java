package Q3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server {
	ServerSocket server = null;
	Socket client = null;
	DataOutputStream out = null;
	DataInputStream in = null;

	public Server() {

		try {
			server = new ServerSocket(5000);
			System.out.println("Server start!");
			client = server.accept();
			out = new DataOutputStream(client.getOutputStream());
			in = new DataInputStream(client.getInputStream());
			String message = null;
			while (true) {
				message = in.readUTF();
				System.out.println("Receive from MyClient: " + message);
				if (message.equalsIgnoreCase("hello")) {
					out.writeLong(System.currentTimeMillis());
					out.flush();
					break;
				}
			}
		} catch (SocketException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			close();
		}
	}

	private void close() {
		System.out.println("Server close!");
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (in != null) {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (client != null) {
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (server != null) {
				server.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Server();
	}
}
