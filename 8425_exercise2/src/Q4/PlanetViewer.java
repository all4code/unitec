package Q4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class PlanetViewer extends JFrame implements ActionListener {

	private static final long serialVersionUID = -2097921611918913104L;

	JPanel jpLeft, jpRight;
	JScrollPane jsp;
	JTextArea jta;
	JRadioButton jrbMercury, jrbVenus, jrbEarth, jrbMars;
	ButtonGroup br;
	JButton jbtnExit;
	Font font;
	String mercuryDesc, venusDesc, earthDesc, marsDesc;

	public PlanetViewer() {
		this.setTitle("Planet Viewer");
		this.setSize(520, 250);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exit();
			}
		});

		jpLeft = new JPanel();
		jpLeft.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		jpLeft.setSize(350, 200);

		jpRight = new JPanel();
		jpRight.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		jpRight.setBackground(Color.WHITE);
		jpRight.setLayout(new GridLayout(5, 1));
		jpRight.setSize(150, 200);

		mercuryDesc = "Mercury is the closest planet to the Sun and due to its proximity " 
				+ "it is not easily seen except during twilight. For every two orbits " 
				+ "of the Sun, Mercury completes three rotations about its axis and " 
				+ "up until 1965 it was thought that the same side of Mercury constantly " 
				+ "faced the Sun. Thirteen times a century Mercury can be observed from " 
				+ "the Earth passing across the face of the Sun in an event called a "
				+ "transit, the next will occur on the 9th May 2016.";

		venusDesc = "Venus is the second planet from the Sun and is the second brightest " 
				+ "object in the night sky after the Moon. Named after the Roman goddess " 
				+ "of love and beauty, Venus is the second largest terrestrial planet and " 
				+ "is sometimes referred to as the Earth’s sister planet due the their " 
				+ "similar size and mass. The […]";

		earthDesc = "Earth is the third planet from the Sun and is the largest of the " 
				+ "terrestrial planets. The Earth is the only planet in our solar system " 
				+ "not to be named after a Greek or Roman deity. The Earth was formed " 
				+ "approximately 4.54 billion years ago and is the only known planet to " 
				+ "support life. Planet […]";

		marsDesc = "Since the first spacecraft was sent to Mars was launched in 1960, " 
				+ "there have been at least 68 missions that have been launched to the Red " 
				+ "Planet or have flown by it on their way to other solar system bodies. " 
				+ "If you count orbiting telescopes such as Hubble Space Telescope that " 
				+ "have looked at Mars […]";

		jta = new JTextArea(mercuryDesc);
		jta.setLineWrap(true);
		jta.setWrapStyleWord(true);
		jta.setEditable(false);
		jsp = new JScrollPane(jta);
		jsp.setPreferredSize(new Dimension(350, 200));
		jsp.setAlignmentX(JScrollPane.LEFT_ALIGNMENT);
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jpLeft.add(jsp);

		jrbMercury = new JRadioButton("Mercury", true);
		jrbMercury.addActionListener(this);

		jrbVenus = new JRadioButton("Venus");
		jrbVenus.addActionListener(this);

		jrbEarth = new JRadioButton("Earth");
		jrbEarth.addActionListener(this);

		jrbMars = new JRadioButton("Mars");
		jrbMars.addActionListener(this);

		jbtnExit = new JButton("Exit");
		jbtnExit.addActionListener(this);

		br = new ButtonGroup();
		br.add(jrbMercury);
		br.add(jrbVenus);
		br.add(jrbEarth);
		br.add(jrbMars);

		jpRight.add(jrbMercury);
		jpRight.add(jrbVenus);
		jpRight.add(jrbEarth);
		jpRight.add(jrbMars);
		jpRight.add(jbtnExit);

		getContentPane().add(jpLeft, BorderLayout.WEST);
		getContentPane().add(jpRight, BorderLayout.EAST);
		pack();
		setVisible(true);

	}

	private void exit() {
		System.exit(0);
	}

	public static void main(String[] args) {
		new PlanetViewer();
	}

	private void setTextArea(String msg) {
		jta.setText(msg);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == jrbMercury) {
			setTextArea(marsDesc);
		} else if (obj == jrbVenus) {
			setTextArea(venusDesc);
		} else if (obj == jrbEarth) {
			setTextArea(earthDesc);
		} else if (obj == jrbMars) {
			setTextArea(marsDesc);
		} else if (obj == jbtnExit) {
			exit();
		}
	}
}
