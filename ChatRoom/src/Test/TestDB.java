package Test;


import static org.junit.Assert.assertTrue;

import org.junit.Test;

import server.User;
import server.UserDBUtilities;


public class TestDB {

	@Test
	public void testIsRegisteredUser() {
		String username = "aaa";
		String password = "123";
		assertTrue("He is a registered user",UserDBUtilities.isRegisteredUser(username, password));
	}
	
	@Test
	public void testRegisteredUser() {
		String username = "ddd";
		String password = "123";
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		assertTrue("A new user should be registered",UserDBUtilities.registerUser(user));
	}
	
	@Test
	public void testListUsers() {
		UserDBUtilities.listAllUsers();
	}

}
