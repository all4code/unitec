package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import common.Constants;

/**
 * The client of ChatRoom
 * 
 * @author po
 *
 */
public class ChatClient {
	Socket client;
	DataInputStream dis;
	DataOutputStream dos;
	String serverPort;

	public ChatClient() {
		try {
			client = new Socket(Constants.SERVER_HOST_ADDRESS, Constants.SERVER_SOCKET_PORT);
			dis = new DataInputStream(client.getInputStream());
			dos = new DataOutputStream(client.getOutputStream());
			
			// define a ServerScoket port for client to receive files
			serverPort = Constants.SERVER_SOCKET_PORT_1;
			
			new LoginFrame(this);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public DataInputStream getDis() {
		return dis;
	}

	public DataOutputStream getDos() {
		return dos;
	}

	public String getServerPort() {
		return serverPort;
	}

	protected void close() {
		try {
			if (dis != null)
				dis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (dos != null)
				dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (client != null)
				client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	public static void main(String[] args) {
		new ChatClient();
	}
}
