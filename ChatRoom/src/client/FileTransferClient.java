package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.JOptionPane;

import common.Constants;
import common.Utilities;

/**
 * Sending file to other clients
 * @author po
 *
 */

public class FileTransferClient implements Runnable {

	private Socket target;
	
	//input/output of streams for communicating
	private DataInputStream in;
	private DataOutputStream out;
	
	//input/output of streams for file transferring
	private DataInputStream fin;
	private DataOutputStream fout;
	
	//get main frame instance
	private MainFrame mainFrame;
	
	//to store received file
	private File file;
	
	//a flag of thread running
	private boolean stop = false;

	public FileTransferClient(MainFrame mainFrame, File file, String targetIP, String targetPort) {
		try {
			
			this.file = file;
			this.mainFrame = mainFrame;
			this.target = new Socket(targetIP, Integer.valueOf(targetPort));
			System.out.println("client connected with target: " + targetIP + ", port:" + targetPort);
			
			//init streams
			in = new DataInputStream(target.getInputStream());
			out = new DataOutputStream(target.getOutputStream());
			
			fin = new DataInputStream(new FileInputStream(file));
			fout = new DataOutputStream(target.getOutputStream());
			
			//request target prepare a folder to save the file and then provide the filename to target
			out.writeInt(Constants.REQUEST_FILE_PATH);
			out.writeUTF(file.getName() + Constants.MSG_FIELD_SEPARATOR + String.valueOf(file.length()));
			out.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void run() {
		try {
			while (!stop) {
				if (stop) {
					break;
				}
				int mesageType = in.readInt();
				switch (mesageType) {
				// receive a message of the file path has been prepared
				// so file sending can be triggered
				case Constants.FILE_PATH_PREPARED:
					System.err.println("FILE_PATH_PREPARED");
					sendFile(file);
					break;
					
				// receive a message of file sending has been cancelled
				case Constants.FILE_TRANSFER_CANCEL:
					System.err.println("FILE_TRANSFER_CANCEL");
					JOptionPane.showMessageDialog(mainFrame, "File receiving has been cancelled");
					close();
					break;
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
			close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// sending file
	private void sendFile(File file) {
		mainFrame.setClientBusy(true);
		// get defined foreground color of the JProgressbar in main frame
		mainFrame.setJpgBarForeground(Utilities.getSendingColor());
		
		
		
		int bufferSize = Constants.CLIENT_BUFFER_SIZE;
		byte[] buf = new byte[bufferSize];
		int len = 0;
		long writtenLength = 0L;

		
		try {
			out.writeInt(Constants.START_RECEIVE_FILE);
			
			while ((len = fin.read(buf)) != -1) {
			
				writtenLength += len;
				
				// to set the progressing value to the JProgressbar in main frame
				mainFrame.setJpgBarValue(Utilities.getPercent(writtenLength, file.length()));
				
				fout.write(buf, 0, len);
		
			}
			
			fin.close();
			fout.flush();
			fout.close();
			
			JOptionPane.showMessageDialog(mainFrame, "Hi, " + mainFrame.getUsername()+  ", The file has benn sent successfully");

			// set JProgressbar looks like its initial status
			mainFrame.setJpgBarString("");
			mainFrame.setJpgBarValue(0);
			mainFrame.setClientBusy(false);
			close();

		} catch (SocketException e) {
			System.out.println("The connection stop!");
			close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	private void close() {
		stop = true;
		
		try {
			if (in != null) {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			if (fin != null) {
				fin.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			if (fout != null) {
				fout.close();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			if (target != null) {
				target.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
