package client;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import common.Constants;
import common.Utilities;

/**
 * receive file sent from other clients
 * 
 * @author Po
 *
 */
public class FileTransferHandler implements Runnable, ActionListener {
	private Socket client;
	
	//input/output of streams for communicating
	private DataOutputStream out;
	private DataInputStream in;
	
	//input/output of streams for file transferring
	private DataInputStream fin;
	private DataOutputStream fout;
	
	// to store the transferring file
	private File file;
	
	// a flag of this thread in the while loop
	private boolean stop = false;
	
	// to store file name and path of the transferring file
	private String savePath,filename;
	
	// to store file size of the transferring file
	private long fileLength;
	
	// a JDialog to get a path for file, including save in default location
	// and user selected location
	private JDialog dialog;
	private JButton btnSave, btnSaveAs, btnCancel;
	
	// to store the instance of main frame
	private MainFrame mainFrame;
	
	public FileTransferHandler(Socket client, MainFrame mainFrame) {
		try {
			this.client = client;
			this.mainFrame = mainFrame;
			out = new DataOutputStream(client.getOutputStream());
			in = new DataInputStream(client.getInputStream());
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (!stop) {
				if (stop) {
					break;
				}
				int mesageType = in.readInt();
				String data = null;
				switch (mesageType) {
				
				// other client request a folder to save file
				case Constants.REQUEST_FILE_PATH:
					System.err.println("REQUEST_FILE_PATH");
					data = in.readUTF();

					// send filename and length to target client
					String[] array = Utilities.resolveMsg(data);
					this.filename = array[0];
					this.fileLength = Long.valueOf(array[1]);
					System.out.println("requested transfer file: " + data);

					// a JDialog that get the client selected path
					confirmPath();
					System.out.println("The path prepared is: " + savePath);
					break;
					
				//other client start to send file
				case Constants.START_RECEIVE_FILE:
					System.err.println("START_RECEIVE_FILE");
					
					// the task will cancel if the save path is not prepared
					if (savePath == null || "".equals(savePath.trim())) {
						out.writeInt(Constants.FILE_TRANSFER_CANCEL);
						out.flush();
					} else {
						
						// to start receive file
						receiveFile();
					}
					break;
				}
			}
		} catch (EOFException e) {
			System.out.println("File transfer has been cancelled!");
		} catch (SocketException e) {
			System.out.println("File transfer has been cancelled!");
		} catch (IOException e) {
			System.out.println("File transfer has been cancelled!");
		}finally{
			close();
		}
	}
	
	/*
	 * get a file path based on users' choices
	 */
	private String confirmPath() {
		dialog = new JDialog(mainFrame,true);
		
		dialog.setTitle("Receive File".toUpperCase());
		dialog.setSize(420, 180);
		dialog.setResizable(false);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		//set the location in the middle of main frame
		Point p = mainFrame.getMainFrameLocation();
		Dimension m = mainFrame.getSize();
		int x = p.x + (m.width - 420)/2;
		int y = p.y + (m.height - 180)/2;
		dialog.setLocation(x, y);

		dialog.setLayout(null);
		Font font = Utilities.getDialogFont();

		JLabel label = new JLabel("Hi, " + mainFrame.getUsername() + ", Please choose a place to save file:");
		label.setBounds(10, 10, 400, 70);
		label.setFont(font);

		btnSave = new JButton("save".toUpperCase());
		btnSave.setBounds(10, 90, 120, 30);
		btnSave.setFont(font);

		btnSaveAs = new JButton("save as".toUpperCase());
		btnSaveAs.setBounds(145, 90, 120, 30);
		btnSaveAs.setFont(font);

		btnCancel = new JButton("cancel".toUpperCase());
		btnCancel.setBounds(280, 90, 120, 30);
		btnCancel.setFont(font);

		btnSave.addActionListener(this);
		btnSaveAs.addActionListener(this);
		btnCancel.addActionListener(this);

		dialog.add(label);
		dialog.add(btnSave);
		dialog.add(btnSaveAs);
		dialog.add(btnCancel);

		dialog.setVisible(true);
		return savePath;
	}

	// to retrieve the path based on user's selection 
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();

		try {
			if (obj == btnSave || obj == btnSaveAs) {
				
				if (obj == btnSave) {
					
					savePath = Utilities.getDefaultUploadFolder();
				
				} else if (obj == btnSaveAs) {
				
					JFileChooser chooser = new JFileChooser();
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int returnVal = chooser.showOpenDialog(dialog);

					if (returnVal == JFileChooser.APPROVE_OPTION) {
					
						file = chooser.getSelectedFile();
						
						if (file.isDirectory()) {
						
							savePath = file.getAbsolutePath();
						
						}
					}
				}
				
				out.writeInt(Constants.FILE_PATH_PREPARED);

			} else if (obj == btnCancel) {
				
				savePath = "";
				out.writeInt(Constants.FILE_TRANSFER_CANCEL);
			
			}

			out.flush();

			System.out.println("save path: " + savePath);
			dialog.setVisible(false);

		} catch (HeadlessException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public void receiveFile() {
		mainFrame.setClientBusy(true);
		try {
			byte[] buff = new byte[Constants.CLIENT_BUFFER_SIZE];
			
			// to set a color and texts for the JProgressbar in main frame
			mainFrame.setJpgBarForeground(Utilities.getReceivingColor());
			mainFrame.setJpgBarString(filename + " Receiving ... " );
			
			File receiveFile = new File(savePath + File.separator + filename);
			fin = new DataInputStream(client.getInputStream());
			fout = new DataOutputStream(new FileOutputStream(receiveFile));

			int len = 0;
			
			// to store current written length
			long writtenLength = 0L;

			while ((len = fin.read(buff)) != -1) {
				
				writtenLength += len;
				
				// to set the percent value to the JProgressbar in the main frame
				mainFrame.setJpgBarValue(Utilities.getPercent(writtenLength, fileLength));
				
				fout.write(buff, 0, len);
			}

			fout.flush();
			
			// file transferring finish
			int result = JOptionPane.showConfirmDialog(mainFrame, "Hi " + mainFrame.getUsername() + ", file received succesfully! \n Do you want open the file location?");
			
			// open the location of the received file
			if (result == JOptionPane.YES_OPTION) {
				Runtime.getRuntime().exec("cmd /c start explorer /select," + receiveFile.getPath());
			}
			
			// set the JProgreessbar looks like its initial status
			mainFrame.setJpgBarString("");
			mainFrame.setJpgBarValue(0);
			mainFrame.setClientBusy(false);
		} catch (SocketException e) {
			System.out.println("The connection is stop!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}
	
	private void close() {
		stop = true;
		try {
			if (fout != null) {
				fout.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (fin != null) {
				fin.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (in != null) {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (client != null) {
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
