package client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * As a file download Server, wait clients that wants connect with.
 * 
 * @author Po
 *
 */
public class FileTransferServer implements Runnable{
	
	private ServerSocket listener;
	private Socket client;
	private ArrayList<FileTransferHandler> clientList = null;
	private boolean stop;
	private MainFrame mainFrame;
	
	public FileTransferServer(String downloadPort, MainFrame mainFrame) {
		this.stop = false;
		this.clientList = new ArrayList<FileTransferHandler>();
		this.mainFrame = mainFrame;
		
		try {
			listener = new ServerSocket(Integer.valueOf(downloadPort));
			System.out.println("File server start at: " + listener.getInetAddress().getHostAddress()+ ":" + listener.getLocalPort());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		try {
			while (!stop) {
				if (stop) {
					close();
				}
				client = listener.accept();
				FileTransferHandler dst = new FileTransferHandler(client, mainFrame);
				new Thread(dst).start();
				clientList.add(dst);
			}
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}
	
	private void close() {
		stop = true;
		try {
			if (listener != null) {
				listener.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
