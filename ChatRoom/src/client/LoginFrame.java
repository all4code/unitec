package client;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.SocketException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import common.Constants;
import common.Utilities;

public class LoginFrame extends JFrame implements ActionListener, Runnable {

	private static final long serialVersionUID = 3671533673555983963L;

	private JLabel lblUsername, lblPassword, lblValidateUsername, lblValidatePassword;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private JButton btnLogin, btnClear, btnRegister;
	private Font contentFont, errorFont;
	private boolean runThread;
	private ChatClient client;

	public LoginFrame(ChatClient client) {
		this.client = client;
		this.runThread = true;

		this.setSize(480, 230);
		this.setTitle("Login".toUpperCase());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);

		// set the location in the center of the screen
		int width = Utilities.getMiddleLocationX(this.getWidth());
		int height = Utilities.getMiddleLocationY(this.getHeight());
		this.setLocation(width, height);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);

		Container container = this.getContentPane();

		contentFont = Utilities.getDialogFont();
		errorFont = Utilities.getErrorFont();

		lblUsername = new JLabel("Username:");
		lblUsername.setBounds(20, 30, 130, 25);
		lblUsername.setHorizontalAlignment(JLabel.RIGHT);
		lblUsername.setFont(contentFont);

		lblPassword = new JLabel("Password:");
		lblPassword.setBounds(20, 85, 130, 25);
		lblPassword.setHorizontalAlignment(JLabel.RIGHT);
		lblPassword.setFont(contentFont);

		txtUsername = new JTextField();
		txtUsername.setBounds(160, 30, 267, 25);
		txtUsername.setFont(contentFont);
		txtUsername.addKeyListener(new MyKeyAction());

		lblValidateUsername = new JLabel();
		lblValidateUsername.setForeground(Color.RED);
		lblValidateUsername.setBounds(160, 60, 267, 20);
		lblValidateUsername.setFont(errorFont);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(160, 85, 267, 25);
		txtPassword.setFont(contentFont);
		txtPassword.addKeyListener(new MyKeyAction());

		lblValidatePassword = new JLabel("");
		lblValidatePassword.setForeground(Color.RED);
		lblValidatePassword.setBounds(160, 115, 267, 20);
		lblValidatePassword.setFont(errorFont);

		// buttons
		btnLogin = new JButton("Login".toUpperCase());
		btnLogin.setBounds(32, 145, 130, 30);
		btnLogin.setFont(contentFont);
		btnLogin.addActionListener(this);

		btnClear = new JButton("Clear".toUpperCase());
		btnClear.setBounds(172, 145, 130, 30);
		btnClear.setFont(contentFont);
		btnClear.addActionListener(this);

		btnRegister = new JButton("Register".toUpperCase());
		btnRegister.setBounds(312, 145, 130, 30);
		btnRegister.setFont(contentFont);
		btnRegister.addActionListener(this);

		container.add(lblUsername);
		container.add(lblPassword);

		container.add(txtUsername);
		container.add(lblValidateUsername);

		container.add(txtPassword);
		container.add(lblValidatePassword);

		container.add(btnLogin);
		container.add(btnClear);
		container.add(btnRegister);

		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent event) {
				exit();
			}
		});

		this.setVisible(true);

		new Thread(this).start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();

		if (obj == btnLogin) {
			
			// validate the user input
			validateRegistration();
			
		} else if (obj == btnClear) {
			
			txtUsername.setText("");
			txtPassword.setText("");

			lblValidateUsername.setText("");
			lblValidatePassword.setText("");

		} else if (obj == btnRegister) {
			
			// go to register frame and dispose this thread
			goToAnotherFrame();
			
			new RegisterFrame(client);

		} else {
			System.out.println("no action");
		}
	}

	// client side validation
	private void validateRegistration() {
		// validate user name
		if (txtUsername.getText().length() <= 0) {
			
			lblValidateUsername.setText("The username is required");
		
		} else if (txtUsername.getText().length() > 10) {
		
			lblValidateUsername.setText("The username should be shorter than 10");
		
		} else {
		
			lblValidateUsername.setText("");
		
		}

		// validate password and confirm password
		if (txtPassword.getPassword().length <= 0) {
		
			lblValidatePassword.setText("The password is required");
		
		} else if (txtPassword.getPassword().length > 10) {
		
			lblValidatePassword.setText("The password should be shorter than 10");
		
		} else {
		
			lblValidatePassword.setText("");
			
			try {
				// request server to validate
				client.getDos().writeInt(Constants.LOGIN_CLIENT);
				client.getDos().writeUTF(txtUsername.getText() + Constants.MSG_FIELD_SEPARATOR + Utilities.getPassword(txtPassword.getPassword()));
				client.getDos().flush();
			
			} catch (IOException e) {
			
				e.printStackTrace();
			
			}

		}
	}

	@Override
	public void run() {
		try {
			while (runThread) {
				String data = "";
				// receive a message from the server, determine message
				// type based on an integer
				int messageType = client.getDis().readInt();
				
				// receive a decode message and process
				switch (messageType) {
				
				// server side validation has passed, allow clients login
				case Constants.LOGIN_CLIENT_ACCEPT:
					System.out.println("Login successfully");
					data = client.getDis().readUTF();
				
					//server returns user name, ip address and port 
					String clientTitle = txtUsername.getText() + " " + data;
					
					// run main frame
					new MainFrame(clientTitle, client);
					
					// dispose this frame
					close();
					break;

				// server validation is failed and returns the error message  
				case Constants.LOGIN_CLIENT_REFUSE:
					data = client.getDis().readUTF();
					
					// show error message
					JOptionPane.showMessageDialog(this, data);

					break;
				case Constants.LOGIN_STOP:
					close();
					break;
				}
			}
		} catch (SocketException e) {
			System.out.println("a user exit when logining!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close();
		}

	}

	// set message to server in order to receive Integer message to stop loop
	private void goToAnotherFrame() {
		try {
			client.getDos().writeInt(Constants.LOGIN_TO_REGISTER);
			client.getDos().flush();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private void close() {
		runThread = false;
		this.dispose();
		this.setVisible(false);
	}

	private void exit() {
		try {
			client.getDos().writeInt(Constants.EXIT_MESSAGE);
			client.getDos().flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			client.close();
		}
	}

	// to listen 'Enter' as the same action of pressing 'Login' button
	class MyKeyAction extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				validateRegistration();
			}
		}

	}
}
