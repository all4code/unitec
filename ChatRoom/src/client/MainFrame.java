package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import common.Constants;
import common.Utilities;

public class MainFrame extends JFrame implements ActionListener, Runnable {

	private static final long serialVersionUID = 980389841528802556L;

	// define the user interface components
	private JCheckBox jchxbSelectAll;
	private JTextField chatInput;
	private JTextPane chatHistory;
	private JButton btnSend, btnBrowse, btnAddToBlackList, btnClearBlackList,
			btnDeleteFromBlackList, btnTransfer;
	private JPanel leftPanel, bottomPanel;
	private JList<String> clientList, blackList;
	private DefaultListModel<String> clientListModel, blackListModel;
	private DefaultComboBoxModel<String> clientComboBoxModel;
	private JScrollPane jspClientList, jspBlackList;
	private JFileChooser fileChooser;
	private JLabel lblBlackList;
	private JComboBox<String> jcobClientSelector;
	private JProgressBar jpgBar;

	private Font font;

	// store client information
	private String serverSocketPort, username, ipAddress, port;

	// store the instance of ChatClient
	private ChatClient client;

	private boolean runThread, connectionError;
	private static boolean clientBusy;

	public MainFrame(String username, ChatClient client) {

		this.client = client;
		this.username = username.substring(0, username.indexOf("[")).trim();
		this.ipAddress = username.substring(username.indexOf("[") + 1,
				username.indexOf(":")).trim();
		this.port = username.substring(username.indexOf(":") + 1,
				username.indexOf("]")).trim();
		this.serverSocketPort = client.getServerPort();
		this.font = Utilities.getDefaultFont();
		this.runThread = true;
		this.connectionError = false;
		clientBusy = false;
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(780, 600);
		this.setResizable(false);
		int x = Utilities.getMiddleLocationX(780);
		int y = Utilities.getMiddleLocationY(600);
		this.setLocation(x, y);

		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout());

		this.setTitle(username);

		// left part: client list, black list and buttons of black list
		// operations
		leftPanel = new JPanel();
		leftPanel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		// the spaces between this component and others is
		// top(5),left(5),bottom(0),right(5)
		c.insets = new Insets(5, 5, 0, 5);

		// automatically fill space, horizontally
		c.fill = GridBagConstraints.HORIZONTAL;

		// start at first line
		c.anchor = GridBagConstraints.FIRST_LINE_START;

		jchxbSelectAll = new JCheckBox("Select All".toUpperCase());
		jchxbSelectAll.setFont(font);
		jchxbSelectAll.setSelected(true);

		// jcbSelectAll.setPreferredSize(new Dimension(200,30));
		// the last cell in the row (only one)

		// the only one item in a row
		c.gridwidth = GridBagConstraints.REMAINDER;

		// first row and first column
		c.gridx = 0;
		c.gridy = 0;
		leftPanel.add(jchxbSelectAll, c);

		// the spaces between following components are
		// top(0),left(5),bottom(5),right(5)
		c.insets = new Insets(0, 5, 5, 5);

		clientListModel = new DefaultListModel<String>();
		clientList = new JList<String>(clientListModel);
		clientList.setFont(font);
		clientList.setEnabled(false);

		jspClientList = new JScrollPane(clientList,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jspClientList.setPreferredSize(new Dimension(80, 290));

		// second row and first column
		c.gridx = 0;
		c.gridy = 1;
		leftPanel.add(jspClientList, c);

		lblBlackList = new JLabel("Black List".toUpperCase());
		lblBlackList.setFont(font);

		// third row and first column
		c.gridx = 0;
		c.gridy = 2;
		leftPanel.add(lblBlackList, c);

		JPanel jpanel = new JPanel();
		btnAddToBlackList = new JButton("Add".toUpperCase());
		btnAddToBlackList.setPreferredSize(new Dimension(80, 30));

		btnDeleteFromBlackList = new JButton("Delete".toUpperCase());
		btnDeleteFromBlackList.setPreferredSize(new Dimension(80, 30));

		btnClearBlackList = new JButton("Clear".toUpperCase());
		btnClearBlackList.setPreferredSize(new Dimension(80, 30));

		jpanel.add(btnAddToBlackList);
		jpanel.add(btnDeleteFromBlackList);
		jpanel.add(btnClearBlackList);

		// fourth row and first column
		c.gridx = 0;
		c.gridy = 3;
		leftPanel.add(jpanel, c);

		blackListModel = new DefaultListModel<String>();
		blackList = new JList<String>(blackListModel);
		blackList.setFont(font);

		jspBlackList = new JScrollPane(blackList,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jspBlackList.setPreferredSize(new Dimension(80, 80));

		// fifth row and first column
		c.gridx = 0;
		c.gridy = 4;
		leftPanel.add(jspBlackList, c);

		contentPane.add(leftPanel, BorderLayout.WEST);

		// chat text area
		chatHistory = new JTextPane();
		chatHistory.setEditable(false);
		chatHistory.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(this.getBackground(), 5),
				BorderFactory.createLineBorder(Color.BLACK)));
		contentPane.add(new JScrollPane(chatHistory), BorderLayout.CENTER);

		// bottom part
		bottomPanel = new JPanel();
		bottomPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		bottomPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints();

		// the spaces between following components are
		// top(5),left(5),bottom(5),right(5)
		c.insets = new Insets(5, 5, 5, 5);

		// auto fill spaces, horizontally
		c.fill = GridBagConstraints.HORIZONTAL;

		chatInput = new JTextField();
		chatInput.setPreferredSize(new Dimension(580, 30));
		chatInput.setFont(font);

		// the component take 6 columns
		c.gridwidth = 6;

		// first row and first column
		c.gridx = 0;
		c.gridx = 0;
		bottomPanel.add(chatInput, c);

		btnSend = new JButton("Send".toUpperCase());
		btnSend.setPreferredSize(new Dimension(100, 30));

		// the last column in the row
		c.gridwidth = GridBagConstraints.REMAINDER;

		// first row and seventh column
		c.gridx = 6;
		c.gridy = 0;
		bottomPanel.add(btnSend, c);

		// the very last row
		btnBrowse = new JButton("browse".toUpperCase());
		btnBrowse.setPreferredSize(new Dimension(100, 30));

		// takes one column wide
		c.gridwidth = 1;

		// second row and first column
		c.gridx = 0;
		c.gridy = 1;
		bottomPanel.add(btnBrowse, c);

		jpgBar = new JProgressBar();
		jpgBar.setPreferredSize(new Dimension(350, 30));
		jpgBar.setFont(font);
		jpgBar.setMinimum(0);
		jpgBar.setMaximum(100);
		jpgBar.setStringPainted(true);
		jpgBar.setBorderPainted(true);
		jpgBar.setOrientation(SwingConstants.HORIZONTAL);
		jpgBar.setString("");
		jpgBar.setBorder(BorderFactory.createEtchedBorder());

		// following components takes three columns
		c.gridwidth = 3;

		// second row and second column
		c.gridx = 1;
		c.gridy = 1;
		bottomPanel.add(jpgBar);

		clientComboBoxModel = new DefaultComboBoxModel<String>();
		jcobClientSelector = new JComboBox<String>(clientComboBoxModel);
		jcobClientSelector.setFont(font);
		jcobClientSelector.setPreferredSize(new Dimension(150, 30));
		c.gridwidth = 2;

		// second row and fifth column
		c.gridx = 4;
		c.gridy = 1;
		bottomPanel.add(jcobClientSelector, c);

		btnTransfer = new JButton("Transfer".toUpperCase());
		btnTransfer.setPreferredSize(new Dimension(100, 30));
		c.gridwidth = 1;

		// second row and seventh column
		c.gridx = 6;
		c.gridy = 1;
		bottomPanel.add(btnTransfer, c);

		contentPane.add(bottomPanel, BorderLayout.SOUTH);

		setVisible(true);

		// events

		// SELECTED: send public message, disable the client list
		// DESELECTED: send private message
		jchxbSelectAll.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					clientList.setEnabled(false);
				} else {
					clientList.setEnabled(true);
				}
			}

		});

		// system exit when the window close
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent event) {
				close();
			}
		});

		// add button events
		btnAddToBlackList.addActionListener(this);
		btnDeleteFromBlackList.addActionListener(this);
		btnClearBlackList.addActionListener(this);
		btnBrowse.addActionListener(this);
		btnTransfer.addActionListener(this);
		btnSend.addActionListener(this);
		chatInput.addActionListener(this);

		// define a thread to take care of messages sent from the server
		new Thread(this).start();

		// define a thread to take care of file sent from other client
		new Thread(new FileTransferServer(this.serverSocketPort, this)).start();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		try {
			Object obj = event.getSource();
			if (obj == btnSend || obj == chatInput ) {
				String chatMsg = chatInput.getText();
				List<String> selectedUser = clientList.getSelectedValuesList();

				// when client type nothing in the text box
				if (chatMsg == null || chatMsg.trim().equals("")) {

					JOptionPane.showMessageDialog(this,
							"No message can be found!", "Warning",
							JOptionPane.WARNING_MESSAGE);

				}
				// when client type 'exit' message, a warning message shows
				else if (chatMsg.trim().equals(Constants.CLIENT_EXIT_MSG)) {

					int action = JOptionPane.showConfirmDialog(this,
							"Do you want to exit?");

					if (action == JOptionPane.YES_OPTION) {
						client.getDos().writeInt(Constants.EXIT_MESSAGE);
						client.getDos().writeUTF(username);
						close();
						System.exit(NORMAL);
					}
				}
				// when both 'SELECT ALL' and client names are not
				// checked, a warning message will be shown
				else if (selectedUser.size() <= 0
						&& !jchxbSelectAll.isSelected()) {
					JOptionPane.showMessageDialog(this,
							"Please choose users who you want to talk to!",
							"Warning", JOptionPane.WARNING_MESSAGE);
				} else if (clientListModel.size() <= 0) {
					JOptionPane.showMessageDialog(this,
							"The client list is empty!", "Warning",
							JOptionPane.WARNING_MESSAGE);
				}
				// to judge the message whether the message send to all clients
				// or to specific users
				else {

					boolean isAll = false;
					String strSelectedUser = "";

					// when 'SELECT ALL' is checked
					if (this.jchxbSelectAll.isSelected()) {

						isAll = true;

					} else {
						// get a user list of private message
						for (String user : selectedUser) {

							strSelectedUser += user
									+ Constants.MSG_FIELD_SEPARATOR;

						}

					}

					// based on selection, send message either to all or
					// specific users
					if (isAll) {

						client.getDos().writeInt(Constants.CHAT_MESSAGE);
						client.getDos().writeUTF(chatInput.getText());

					} else {

						client.getDos().writeInt(Constants.PRIVATE_MESSAGE);
						client.getDos().writeUTF(
								Constants.PRIVATE_USER_LIST_START
										+ strSelectedUser
										+ Constants.PRIVATE_USER_LIST_END
										+ chatInput.getText());

					}

					client.getDos().flush();

				}

				chatInput.setText("");

			} else if (obj == btnAddToBlackList) {

				// get selected clients from client list
				List<String> selectedClients = clientList
						.getSelectedValuesList();

				if (selectedClients.size() > 0) {

					// each client be added to black list, they will disappear
					// from client list
					removeFromClientList(selectedClients);

					// update black list
					addToBlackList(selectedClients);
					updateClientComboBox();

					client.getDos().writeInt(Constants.UPDATE_BLACK_LIST);
					client.getDos()
							.writeUTF(
									Utilities
											.listToMsg(getListFromListModel(blackListModel)));
					client.getDos().flush();

				} else {

					JOptionPane.showMessageDialog(this,
							"Please choose one client at least!");

				}

			} else if (obj == btnClearBlackList) {

				List<String> selectedClients = getListFromListModel(blackListModel);

				// clear all items in the black list
				if (selectedClients.size() > 0) {
					int result = JOptionPane
							.showConfirmDialog(
									this,
									"Do you want clear black list? "
											+ "\n All clients in the blact list will be moved back to client list!");

					if (result == JOptionPane.YES_OPTION) {

						addToClientList(selectedClients);
						blackListModel.clear();
						updateClientComboBox();

						client.getDos().writeInt(Constants.UPDATE_BLACK_LIST);
						client.getDos()
								.writeUTF(
										Utilities
												.listToMsg(getListFromListModel(blackListModel)));
						client.getDos().flush();
					}

				} else {

					JOptionPane.showMessageDialog(this,
							"The black list is empty!");

				}

			} else if (obj == btnDeleteFromBlackList) {
				// get selected clients from black list
				List<String> selectedClients = blackList
						.getSelectedValuesList();

				if (selectedClients.size() > 0) {

					addToClientList(selectedClients);
					removeFromBlackList(selectedClients);
					updateClientComboBox();

					client.getDos().writeInt(Constants.UPDATE_BLACK_LIST);
					client.getDos()
							.writeUTF(
									Utilities
											.listToMsg(getListFromListModel(blackListModel)));
					client.getDos().flush();

				} else {

					JOptionPane.showMessageDialog(this,
							"Please choose one client at least!");

				}

			} else if (obj == btnBrowse) {

				fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

				File file = null;
				int returnVal = fileChooser.showOpenDialog(this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					file = fileChooser.getSelectedFile();
					jpgBar.setString(file.getName());
				}

			} else if (obj == btnTransfer) {

				if (!"".equals(jpgBar.getString().trim())) {

					File file = fileChooser.getSelectedFile();

					if (file != null && file.isFile()) {

						Object targetClient = jcobClientSelector
								.getSelectedItem();

						if (targetClient != null) {

							client.getDos().writeInt(
									Constants.REQUEST_SERVER_TRANSFER_FILE);
							client.getDos().writeUTF(
									file.getName()
											+ Constants.MSG_FIELD_SEPARATOR
											+ (String) targetClient);
							client.getDos().flush();
							System.out.println("Send file: " + file.getName()
									+ Constants.MSG_FIELD_SEPARATOR
									+ (String) targetClient);

						}else{
							
							JOptionPane.showMessageDialog(this,
									"No client has been selected!");
							
						}
					}
				} else {

					JOptionPane.showMessageDialog(this,
							"Please choose a file to send!");

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// remove from black list and appear in client list
	private void removeFromBlackList(List<String> selectedClients) {

		List<String> blackListClients = getListFromListModel(blackListModel);
		blackListClients.removeAll(selectedClients);
		updateJListModel(blackListModel, blackListClients);

	}

	// add to black list and disappear from client list
	private void addToBlackList(List<String> selectedClients) {

		List<String> blackListClients = getListFromListModel(blackListModel);

		// add new client to black list without duplication
		blackListClients.removeAll(selectedClients);
		blackListClients.addAll(selectedClients);

		updateJListModel(blackListModel, blackListClients);

	}

	// add the clients from black list
	private void addToClientList(List<String> selectedClients) {

		List<String> oldClientList = getListFromListModel((DefaultListModel<String>) clientList
				.getModel());
		oldClientList.addAll(selectedClients);
		updateJListModel(clientListModel, oldClientList);

	}

	// remove the clients who are in the black list from client list
	private void removeFromClientList(List<String> selectedClients) {
		List<String> clientList = getListFromListModel(clientListModel);

		clientList.removeAll(selectedClients);
		updateJListModel(clientListModel, clientList);
	}

	// return values as a List from ListModel
	private List<String> getListFromListModel(DefaultListModel<String> model) {

		List<String> list = new ArrayList<String>();

		for (int i = 0; i < model.getSize(); i++) {
			list.add(model.get(i));
		}

		return list;
	}

	// clear all items in the JList and add new values
	private void updateJListModel(DefaultListModel<String> model,
			List<String> newValues) {
		model.clear();

		for (String value : newValues) {
			model.addElement(value);
		}
	}

	// update select box in order to keep the same as client list
	private void updateClientComboBox() {
		List<String> clientList = getListFromListModel(clientListModel);

		clientComboBoxModel.removeAllElements();
		for (String item : clientList) {
			clientComboBoxModel.addElement(item);
		}
	}

	// process messages from the server
	@Override
	public void run() {
		String data = "";
		String[] array = null;

		try {
			while (runThread) {
				// receive a message from the server, determine message
				// type based on an integer
				int messageType = client.getDis().readInt();
				System.out.println("Msg type: " + messageType);

				// decode message and process
				switch (messageType) {

				// receive broadcast message
				case Constants.CHAT_BROADCAST:
					System.err.println("CHAT_BROADCAST");

					append(client.getDis().readUTF());

					break;

				// receive commend to update client list
				case Constants.UPDATE_USERLIST:
					System.err.println("UPDATE_USERLIST");
					data = client.getDis().readUTF();

					final String userList = data;
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							upcateClientList(userList);
						}

					});

					break;
					
				//a system broadcast of other client login
				case Constants.LOGIN_BROADCAST:
					System.err.println("LOGIN_BROADCAST");
					append(client.getDis().readUTF());
					break;
					
				// other client will send file to this user
				case Constants.REQUEST_CLIENT_RECEIVE_FILE:
					System.err.println("REQUEST_CLIENT_RECEIVE_FILE");
					data = client.getDis().readUTF();
					
					//resolve other client name and the file name that will be sent
					if (data != null && !"".equals(data)) {

						// array[0]:filename; array[1]client name
						array = Utilities.resolveMsg(data);
						
						if(isClientBusy()){
							
							client.getDos().writeInt(Constants.CLIENT_BUSY);
							client.getDos().writeUTF(array[1]);
							client.getDos().flush();
							
						}else{
							
							int result = JOptionPane.showConfirmDialog(this, "Hi, "
									+ this.getUsername()
									+ ", Do you want receive file: [" + array[0]
									+ "]\n sent from [" + array[1] + "]?");
							
							// tell server that the server socket port for current client
							if (result == JOptionPane.YES_OPTION) {
								client.getDos().writeInt(
										Constants.CLIENT_ALLOW_RECEIVE_FILE);
								client.getDos().writeUTF(
										this.serverSocketPort
												+ Constants.MSG_FIELD_SEPARATOR
												+ array[1]);
								client.getDos().flush();
							}
						}
					}

					break;

				// client is busy with sending or receiving file
				case Constants.CLIENT_BUSY:
					System.err.println("CLIENT_BUSY");
					data = client.getDis().readUTF();
					JOptionPane.showMessageDialog(this, "Client " + data + " is busy now, please wait!");
					break;
							
				// as a file sender, target client has accept file transfer
				// server send this message to let current client to connect
				// with other client directly
				case Constants.CONNECT_TO_CLIENT:
					System.err.println("CONNECT_TO_CLIENT");
					data = client.getDis().readUTF();
					
					// get target client server socket ip and port
					array = Utilities.resolveMsg(data);
					String targetIP = array[0];
					String targetPort = array[1];
					
					File file = fileChooser.getSelectedFile();
					
					if (file != null && file.isFile()) {
						
						// run a new thread to send file
						new Thread(new FileTransferClient(this, file, targetIP,
								targetPort)).start();
					
					}
					break;
				}
			}
		} catch (SocketException e) {
			System.out.println("Client " + getUsername() + " exit!");
			connectionError = true;
		} catch (IOException e) {
			System.out.println("Server connection error!");
			connectionError = true;
		} finally {
			close();
		}

	}

	// update clients in a JList
	private void upcateClientList(String clientList) {
		String[] clientusersArray = clientList
				.split(Constants.MSG_FIELD_SEPARATOR);
		
		List<String> blackList = getListFromListModel(blackListModel);
		
		clientListModel.clear();
		clientComboBoxModel.removeAllElements();
		
		for (String u : clientusersArray) {
		
			if (!u.equalsIgnoreCase(getUsername())) {
				
				//if a client in this black list, he/she will keep there
				if (blackList.contains(u)) {
				
					continue;
				
				} else {
				
					clientListModel.addElement(u);
					clientComboBoxModel.addElement(u);
				
				}
			}
		}
	}

	// append messages to JTextPane in different style based on suffix of the message
	public void append(String msg) throws RemoteException {
		System.err.println(msg);

		if (msg.endsWith(Constants.PRIVATE_MESG_SUFFIX)) {
		
			setPrivateMsg(msg);
		
		} else if (msg.endsWith(Constants.PUBLIC_MESG_SUFFIX)) {
		
			setPublicMsg(msg);
		
		} else {
		
			setBroadcastMsg(msg);
		
		}
	
		chatHistory.setCaretPosition(chatHistory.getStyledDocument()
				.getLength());
	}

	//set the style for system broadcast message
	private void setBroadcastMsg(String str) {

		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setForeground(attrset, Color.darkGray);
		StyleConstants.setFontSize(attrset, 16);
		
		insert(str, attrset);
	}

	//set the style for private message
	private void setPrivateMsg(String str) {

		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setForeground(attrset, Color.red);
		StyleConstants.setBold(attrset, true);
		StyleConstants.setFontSize(attrset, 16);

		insert(str, attrset);
	}

	//set the style for public message
	private void setPublicMsg(String str) {
		
		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setForeground(attrset, Color.blue);
		StyleConstants.setFontSize(attrset, 16);

		insert(str, attrset);
	}

	//append message
	public void insert(String str, AttributeSet attrset) {
		Document docs = chatHistory.getDocument();

		str = str + "\n";
		try {
			docs.insertString(docs.getLength(), str, attrset);
		} catch (BadLocationException ble) {
			System.out.println("BadLocationException:" + ble);
		}
	}

	//provide some functions to allow other classes accessing
	
	public Point getMainFrameLocation() {
		return this.getLocation();
	}

	public Dimension getMainFrameSize() {
		return this.getSize();
	}

	public int getJpgBarValue() {
		return jpgBar.getValue();
	}

	public void setJpgBarValue(int percent) {
		this.jpgBar.setValue(percent);
	}

	public String getJpgBarString() {
		return jpgBar.getString();
	}

	public void setJpgBarString(String value) {
		this.jpgBar.setString(value);
	}

	public void setJpgBarForeground(Color color) {
		this.jpgBar.setForeground(color);
		;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
	
	public static boolean isClientBusy() {
		return clientBusy;
		
	}

	public void setClientBusy(boolean isBusy) {
		
		clientBusy = isBusy;
		
		if(isBusy){
		
			this.disableFileTransfer();
		
		}else{
		
			this.enableFileTransefer();
		
		}
	}

	private void enableFileTransefer(){
		this.btnBrowse.setEnabled(true);
		this.jcobClientSelector.setEnabled(true);
		this.btnTransfer.setEnabled(true);
	}
	
	private void disableFileTransfer(){
		this.btnBrowse.setEnabled(false);
		this.jcobClientSelector.setEnabled(false);
		this.btnTransfer.setEnabled(false);
	}
	
	private synchronized void close() {
		System.out.println(Thread.currentThread().getName());
		runThread = false;
		exit();
	}

	private void exit() {
		try {
			if (!connectionError) {
				client.getDos().writeInt(Constants.EXIT_MESSAGE);
				client.getDos().flush();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			client.close();
		}
	}
}
