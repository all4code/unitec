package client;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.SocketException;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import common.Constants;
import common.Utilities;

/**
 * Client registration
 * 
 * @author po
 *
 */
public class RegisterFrame extends JFrame implements ActionListener,Runnable {

	private static final long serialVersionUID = 3671533673555983963L;
	
	private JLabel lblUsername, lblPassword, lblConfirmPassword, lblValidateUsername, lblValidatePassword, lblValidateConfirmPassword;
	private JTextField txtUsername;
	private JPasswordField txtPassword, txtConfirmPassword;
	private JButton btnRegister, btnClear;
	private Font contentFont, errorFont;
	private ChatClient client;
	private boolean runThread;

	public RegisterFrame(ChatClient client) {
		this.client = client;
		this.runThread = true;

		this.setSize(480, 300);
		this.setTitle("Register".toUpperCase());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);

		// set the location in the centre of the screen
		int width = Utilities.getMiddleLocationX(this.getWidth());
		int height = Utilities.getMiddleLocationY(this.getHeight());
		this.setLocation(width, height);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);

		Container container = this.getContentPane();

		contentFont = Utilities.getDialogFont();
		errorFont = Utilities.getErrorFont();

		lblUsername = new JLabel("Username:");
		lblUsername.setBounds(10, 30, 160, 25);
		lblUsername.setHorizontalAlignment(JLabel.RIGHT);
		lblUsername.setFont(contentFont);

		lblPassword = new JLabel("Password:");
		lblPassword.setBounds(10, 85, 160, 25);
		lblPassword.setHorizontalAlignment(JLabel.RIGHT);
		lblPassword.setFont(contentFont);

		lblConfirmPassword = new JLabel("Confirm Password:");
		lblConfirmPassword.setBounds(10, 140, 160, 25);
		lblConfirmPassword.setHorizontalAlignment(JLabel.RIGHT);
		lblConfirmPassword.setFont(contentFont);

		txtUsername = new JTextField();
		txtUsername.setBounds(175, 30, 260, 25);
		txtUsername.setFont(contentFont);
		txtUsername.addKeyListener(new MyKeyAction());

		lblValidateUsername = new JLabel();
		lblValidateUsername.setForeground(Color.RED);
		lblValidateUsername.setBounds(175, 60, 260, 20);
		lblValidateUsername.setFont(errorFont);

		txtPassword = new JPasswordField();
		txtPassword.setBounds(175, 85, 260, 25);
		txtPassword.setFont(contentFont);
		txtPassword.addKeyListener(new MyKeyAction());

		lblValidatePassword = new JLabel("");
		lblValidatePassword.setForeground(Color.RED);
		lblValidatePassword.setBounds(175, 115, 260, 20);
		lblValidatePassword.setFont(errorFont);

		txtConfirmPassword = new JPasswordField();
		txtConfirmPassword.setBounds(175, 140, 260, 25);
		txtConfirmPassword.setFont(contentFont);
		txtConfirmPassword.addKeyListener(new MyKeyAction());

		lblValidateConfirmPassword = new JLabel();
		lblValidateConfirmPassword.setForeground(Color.RED);
		lblValidateConfirmPassword.setBounds(175, 170, 260, 20);
		lblValidateConfirmPassword.setFont(errorFont);

		// buttons
		btnRegister = new JButton("Register".toUpperCase());
		btnRegister.setBounds(80, 205, 150, 30);
		btnRegister.setFont(contentFont);
		btnRegister.addActionListener(this);

		btnClear = new JButton("Clear".toUpperCase());
		btnClear.setBounds(244, 205, 150, 30);
		btnClear.setFont(contentFont);
		btnClear.addActionListener(this);

		container.add(lblUsername);
		container.add(lblPassword);
		container.add(lblConfirmPassword);

		container.add(txtUsername);
		container.add(lblValidateUsername);

		container.add(txtPassword);
		container.add(lblValidatePassword);

		container.add(txtConfirmPassword);
		container.add(lblValidateConfirmPassword);

		container.add(btnRegister);
		container.add(btnClear);

		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent event) {
				exit();
			}
		});

		this.setVisible(true);
		
		new Thread(this).start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();

		if (obj == btnRegister) {
			validateRegistration();
		} else if (obj == btnClear) {
			txtUsername.setText("");
			txtPassword.setText("");
			txtConfirmPassword.setText("");
			lblValidateUsername.setText("");
			lblValidatePassword.setText("");
			lblValidateConfirmPassword.setText("");
		} else {
			System.out.println("no action");
		}
	}

	private void validateRegistration() {
		// validate user name
		if (txtUsername.getText().length() <= 0) {
			lblValidateUsername.setText("The username is required");
		} else if (txtUsername.getText().length() > 10) {
			lblValidateUsername.setText("The username should be shorter than 10");
		} else {
			lblValidateUsername.setText("");
		}

		// validate password and confirm password
		if (txtPassword.getPassword().length <= 0) {
			lblValidatePassword.setText("The password is required");
		} else if (txtPassword.getPassword().length > 10) {
			lblValidatePassword.setText("The password should be shorter than 10");
		} else {

			lblValidatePassword.setText("");

			if (txtConfirmPassword.getPassword().length <= 0) {
				lblValidateConfirmPassword.setText("Please confirm your password");
			} else if (!Arrays.equals(txtPassword.getPassword(), txtConfirmPassword.getPassword())) {
				lblValidateConfirmPassword.setText("The password must equal to password");
			} else {
				lblValidateConfirmPassword.setText("");
				try {
					client.getDos().writeInt(Constants.REGISTER_CLIENT);
					client.getDos().writeUTF(txtUsername.getText().trim() + Constants.MSG_FIELD_SEPARATOR + Utilities.getPassword(txtPassword.getPassword()).trim());
					client.getDos().flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void close() {
		runThread = false;
		this.setVisible(false);
		this.dispose();
	}

	private void exit() {
		try {
			client.getDos().writeInt(Constants.EXIT_MESSAGE);
			client.getDos().flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close();
			client.close();
		}
	}

	@Override
	public void run() {
		try {
			while (runThread) {
				System.out.println(Thread.currentThread().getName());
				String data = "";
				// receive a message from the server, determine message
				// type based on an integer
				
				int messageType = client.getDis().readInt();
				System.out.println("Msg type: " + messageType);

				switch (messageType) {

				// server side validation has passed, allow clients login
				case Constants.REGISTER_USER_SUCCESS:
					System.out.println("REGISTER_USER_SUCCESS");
					data = client.getDis().readUTF();
					
					//server returns user name, ip address and port 
					String clientTitle = txtUsername.getText() + " " + data;
					
					// run main frame
					new MainFrame(clientTitle, client);
					
					// dispose this frame
					close();
					
					break;
					
				// server validation has found that the name has already been in
				// database and returns the error message
				case Constants.REGISTER_USER_EXIST:
					System.out.println("REGISTER_USER_EXIST");
					data = client.getDis().readUTF();
					
					System.out.println(data);
					
					JOptionPane.showMessageDialog(this, data);
					break;
				}
			}
		} catch (SocketException e) {
			System.out.println("a new user exit when registering!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close();
		}

	}

	class MyKeyAction extends KeyAdapter{

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				validateRegistration();
			}
		}
		
	}
}
