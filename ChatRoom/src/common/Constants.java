package common;

public interface Constants {
	
	// command types
	public static final int CHAT_MESSAGE = 0;
	public static final int EXIT_MESSAGE = 1;
	public static final int CHAT_BROADCAST = 2;
	public static final int PRIVATE_MESSAGE = 3; 
	public static final int REGISTER_CLIENT = 4; 
	public static final int LOGIN_BROADCAST = 5; 
	public static final int LOGIN_CLIENT = 6;

	public static final int LOGIN_CLIENT_ACCEPT = 7;
	public static final int LOGIN_CLIENT_REFUSE = 8;
	
	public static final int UPDATE_USERLIST = 9; 
	
	//client exit before login or register
	public static final int EXIT_MESSAGE_BEFORE_LOGIN = 10;
	
	//for server side validation
	public static final int REGISTER_USER_EXIST = 11;
	public static final int REGISTER_USER_SUCCESS = 12;

	// client request to send files
	public static final int REQUEST_SERVER_TRANSFER_FILE = 13;
	
	// server ask client whether accept file transfer or not
	public static final int REQUEST_CLIENT_RECEIVE_FILE = 14;
	
	// client allow other send file
	public static final int CLIENT_ALLOW_RECEIVE_FILE = 15;
	
	// server inform client to connect the file sending destination
	public static final int CONNECT_TO_CLIENT = 16;
	
	// client ask target to prepare a folder to save the file
	public static final int REQUEST_FILE_PATH = 17;
	
	// target answer the folder has been prepared
	public static final int FILE_PATH_PREPARED = 18;
	
	// target answer the file transferring cancelled
	public static final int FILE_TRANSFER_CANCEL = 19;
	
	// client start send file and target start to receive file
	public static final int START_RECEIVE_FILE = 20;
	
	public static final int TRANSFER_FILE_SUCCESS = 21;
	
	public static final int LOGIN_TO_REGISTER = 22;
	public static final int LOGIN_STOP = 23;
	
	// every time clients change the items in their black list, they need send this
	// to let server know who are in their black list
	public static final int UPDATE_BLACK_LIST = 24;
	
	//when receiving a file, the client is busy and unable to deal with another file transfer request 
	public static final int CLIENT_BUSY = 25;
	
	// options of client server socket ports	
	public static final String SERVER_SOCKET_PORT_1 = "44444";
	public static final String SERVER_SOCKET_PORT_2 = "55555";
	public static final String SERVER_SOCKET_PORT_3 = "33333";
	public static final String SERVER_SOCKET_PORT_4 = "22222";
	public static final String SERVER_SOCKET_PORT_5 = "11111";
	
	// the separator of message
	public static final String MSG_FIELD_SEPARATOR = "&=&";
	
	// the separator of message, e.g. aaaa: xxxx
	public static final String USERNAME_MESSAGE_SEPARATOR = ": ";
	
	// give a name for system message, e.g. SYSTEM: xxxx
	public static final String SYSTEM_MESG_NAME = "SYSTEM";
	
	public static final String SERVER_HOST_ADDRESS = "localhost";
	
	public static final int SERVER_SOCKET_PORT = 4000;
	
	//the buffer size of byte array for transferring files
	public static final int CLIENT_BUFFER_SIZE = 2048;
	
	//define the number of thread in fixed thread pool
	public static final int THREAD_POOL_SIZE = 10;
	
	/*
	 *  for dividing the private users and message
	 *  ths message format is:
	 *  PRIVATE_USER_LIST_STARTuser1[xxx.xxx.xxx.xxx],user2[xxx.xxx.xxx.xxx],PRIVATE_USER_LIST_ENDmessage
	 */
	public static final String PRIVATE_USER_LIST_START = "PRIVATE_USER_LIST_START";
	public static final String PRIVATE_USER_LIST_END = "PRIVATE_USER_LIST_END";
	
	//message type
	public static final String PUBLIC_MESG_SUFFIX = " [PUBLIC]";
	public static final String PRIVATE_MESG_SUFFIX = " [PRIVATE]";
	
	//default folder for saving file
	public static final String DEFAULT_UPLOAD_FOLDER = "uploads";
	
	//the command for exit system
	public static final String CLIENT_EXIT_MSG = "exit";
}
