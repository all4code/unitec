package common;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.util.List;

public class Utilities {
	public static int getMiddleLocationX(int width) {
		return ((int) (getScreenDimension().getWidth()) - width) / 2;
	}

	public static int getMiddleLocationY(int height) {
		return ((int) (getScreenDimension().getHeight()) - height) / 2;
	}

	private static Dimension getScreenDimension() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}

	public static String getPassword(char[] chars) {
		return new String(chars);
	}

	public static String[] resolveMsg(String msg) {
		if (msg == null || "".equals(msg.trim())) {
			return null;
		} else {
			return msg.split(Constants.MSG_FIELD_SEPARATOR);
		}
	}

	public static String listToMsg(List<String> list) {

		if (list == null || list.size() <= 0) {

			return "";

		} else {

			StringBuilder builder = new StringBuilder();

			for (String s : list) {
				builder.append(s + Constants.MSG_FIELD_SEPARATOR);
			}
			return builder.toString();
		}
	}

	public static Font getDefaultFont() {
		return new Font(null, Font.PLAIN, 14);
	}

	public static Font getErrorFont() {
		return new Font(null, Font.PLAIN, 12);
	}

	public static Font getDialogFont() {
		return new Font(null, Font.PLAIN, 18);
	}

	public static String getProjectRootFolder() {
		return System.getProperty("user.dir");
	}

	public static String getDefaultUploadFolder() {
		return getProjectRootFolder() + File.separator
				+ Constants.DEFAULT_UPLOAD_FOLDER;
	}

	public static Color getSendingColor() {
		return Color.pink;
	}

	public static Color getReceivingColor() {
		return new Color(163, 184, 204);
	}

	public static int getPercent(long x, long y) {
		float dx = x * 1.0F;
		float dy = y * 1.0F;
		float by = dx / dy;
		return Math.round(by * 100);
	}

}
