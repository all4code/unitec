package server;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import common.Constants;

public class ChatServer {
        
    public static void main(String[] args)
    {
        try {
            ServerSocket server = new ServerSocket(Constants.SERVER_SOCKET_PORT);
            System.out.println("Server setup: waiting for client connections");
            
            ArrayList<ClientHandler> clientHandlers = new ArrayList<ClientHandler>();
            
            ExecutorService threadPool = Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);
            
            while(true)
            {
                // keep processing and accepting client connections forever
                Socket serverSocket = server.accept();
                
                ClientHandler newClientHandler = new ClientHandler(serverSocket,clientHandlers);
                threadPool.execute(newClientHandler);
                clientHandlers.add(newClientHandler);
            }
            
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
