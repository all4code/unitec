package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import common.Constants;
import common.Utilities;

public class ClientHandler extends Thread {

	DataInputStream dis;
	DataOutputStream dos;

	Socket remoteClient;

	// keep track of all the other clients connected to the Server
	ArrayList<ClientHandler> connectedClients; 
	
	String username, clientIPAddress, clientSocketPort;

	// store clients' blacklist
	List<String> userBlackList;

	private boolean stop = false;

	public ClientHandler(Socket remoteClient, ArrayList<ClientHandler> connectedClients) {
		this.remoteClient = remoteClient;
		this.connectedClients = connectedClients;

		try {
			
			this.dis = new DataInputStream(remoteClient.getInputStream());
			this.dos = new DataOutputStream(remoteClient.getOutputStream());
	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {

		String data = null;
		ClientHandler handler = null;
		
		try {
			while (!stop) {
				// read the type of message from the client
				int mesgType = dis.readInt(); 
				System.err.println(mesgType);

				// decode the message type based on the integer sent from the client
				switch (mesgType) {
				
				//receive chat message from clients
				case Constants.CHAT_MESSAGE:
					System.err.println("CHAT_MESSAGE");
					data = dis.readUTF();
					System.out.println(data);
					
					//broadcast the message to other clients exclude the sender
					broadcastMsg(getUsername() + Constants.USERNAME_MESSAGE_SEPARATOR + data, false);
					break;
					
				//receive client registration requests
				case Constants.REGISTER_CLIENT:
					System.err.println("REGISTER_CLIENT");
					// handle new client registrations
					data = dis.readUTF();
					System.err.println(data);

					//validate name
					if (isExistUsername(data)) {

						getDos().writeInt(Constants.REGISTER_USER_EXIST);
						getDos().writeUTF("The username is exist!");
						getDos().flush();
					
					} else {
						
						// save user name and password to database
						boolean flag = registerClient(data);
						
						if (flag) {
							
							//save client information
							setClientIPAddress(remoteClient.getInetAddress().getHostAddress());
							setClientSocketPort(String.valueOf(remoteClient.getPort()));
							setUsername(getUsernameFromInfo(data));
							
							getDos().writeInt(Constants.REGISTER_USER_SUCCESS);
							getDos().writeUTF(getClientFullPath());
							getDos().flush();

							// the system message is like: SYSTEM: [XXX] joins!
							broadcastMsg(Constants.SYSTEM_MESG_NAME + Constants.USERNAME_MESSAGE_SEPARATOR + "A new user [" + getUsername() + "] joins!", true);

							//force clients to update their client list
							broadcastClientList();
						}
					}
					
					break;
					
				//receive client login requests
				case Constants.LOGIN_CLIENT:
					System.err.println("LOGIN_CLIENT");
					data = dis.readUTF();
					System.out.println(data);

					/*
					 * isRegisteredUser: to match the username and password in
					 * the database isLogin: to judge whether the client login
					 * or not
					 */
					if (isRegisteredUser(data) && !isLogin(data)) {
						
						//save client information
						setUsername(getUsernameFromInfo(data));
						setClientIPAddress(remoteClient.getInetAddress().getHostAddress());
						setClientSocketPort(String.valueOf(remoteClient.getPort()));
						
						getDos().writeInt(Constants.LOGIN_CLIENT_ACCEPT);
						getDos().writeUTF(getClientFullPath());
						getDos().flush();

						System.out.println("client full path: " + getClientFullPath());

						// the system message is like: SYSTEM: [XXX] joins!
						broadcastMsg(Constants.SYSTEM_MESG_NAME + Constants.USERNAME_MESSAGE_SEPARATOR + "[" + getUsername() + "] joins!", true);

						//force clients to update their client list
						broadcastClientList();
					} else {
						
						getDos().writeInt(Constants.LOGIN_CLIENT_REFUSE);
						getDos().writeUTF("Username or password is incorrect or the client have already login!");
						getDos().flush();
						
					}
					
					break;
					
				//client black list has been changed
				case Constants.UPDATE_BLACK_LIST:
					System.err.println("UPDATE_BLACK_LIST");
					data = dis.readUTF();
					System.out.println(data);
					
					//save new black list
					setUserBlackList(data);
					System.out.println(this.getUsername() + "'s black list: " + Utilities.listToMsg(getUserBlackList()));
					
					//force client to update their client list
					broadcastClientList();
					
					break;
				
				//receive private message from clients
				case Constants.PRIVATE_MESSAGE:
					System.err.println("PRIVATE_MESSAGE");
					// to handle private messages sent by the client
					data = dis.readUTF();
					System.out.println(data);
					
					//send to specified private users
					sendPrivateMsg(data);

					break;
					
				// client exit
				case Constants.EXIT_MESSAGE:
					System.err.println("EXIT_MESSAGE");
					
					//remove client information save on system
					removeClient();

					//inform all clients the user exit
					// the system message is like: SYSTEM: [XXX] exit!
					broadcastMsg(Constants.SYSTEM_MESG_NAME + Constants.USERNAME_MESSAGE_SEPARATOR + "[" + getUsername() + "] exit!", true);

					//force client to update their client list
					broadcastClientList();
					break;
					
				//client from login frame move to register frame
				case Constants.LOGIN_TO_REGISTER:
					System.err.println("EXIT_MESSAGE_BEFORE_LOGIN");
					
					//server send command in order to break the loop in login frame
					//because the loop blocks for reading
					dos.writeInt(Constants.LOGIN_STOP);
					dos.flush();

					break;
				
				//client exit before login
				case Constants.EXIT_MESSAGE_BEFORE_LOGIN:
					System.err.println("EXIT_MESSAGE_BEFORE_LOGIN");
					
					//remove information on system, it doesn't need to inform other clients
					removeClient();

					break;
				
				//client request sending file to other users
				case Constants.REQUEST_SERVER_TRANSFER_FILE:
					System.err.println("TRANSFER_FILE");
					data = dis.readUTF();
					System.out.println("data: " + data);

					// array[0]: filename, array[1]:target client name
					if (data != null && !"".equals(data)) {

						String[] array = data.split(Constants.MSG_FIELD_SEPARATOR);
						sendRequest(array[0], array[1]);
				
					}
					
					break;
					
				//when client is busy with receiving or sending files
				case Constants.CLIENT_BUSY:
					System.err.println("CLIENT_BUSY");
					data = dis.readUTF();
					
					//answer sender the client is busy
					handler = getClientHandler(data);
					handler.getDos().writeInt(Constants.CLIENT_BUSY);
					handler.getDos().writeUTF(this.getUsername());
					handler.getDos().flush();
					break;
				//client allow sending file
				case Constants.CLIENT_ALLOW_RECEIVE_FILE:
					System.err.println("CLIENT_ALLOW_RECEIVE_FILE");
					data = dis.readUTF();
					System.out.println(data);
					
					// array[0]: serverPort array[1]:target client
					String[] array = Utilities.resolveMsg(data);
					
					// inform sender to connect with other user
					handler = getClientHandler(array[1]);
					handler.getDos().writeInt(Constants.CONNECT_TO_CLIENT);
					handler.getDos().writeUTF(this.getClientIPAddress() + Constants.MSG_FIELD_SEPARATOR + array[0]);
					handler.getDos().flush();
					
					System.out.println(this.getClientIPAddress() + Constants.MSG_FIELD_SEPARATOR + array[0]);
					
					
				}
			}
		} catch (SocketException e) {
			System.out.println("client [" + getUsername() + "] left!");
		} catch (EOFException e) {
			System.out.println("client [" + getUsername() + "] left!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close();
		}

	}

	//send request of file transfer
	private void sendRequest(String filename, String targetClient) {
		try {

			ClientHandler handler = getClientHandler(targetClient);
			handler.getDos().writeInt(Constants.REQUEST_CLIENT_RECEIVE_FILE);
			handler.getDos().writeUTF(filename + Constants.MSG_FIELD_SEPARATOR + this.getUsername());
			handler.getDos().flush();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * if it is system message, it will send to everybody, otherwise,
	 * doesn't send the message to the client that sent the message in the first
	 * place
	 */
	private void broadcastMsg(String msg, boolean isSystemMsg) throws IOException {
		System.out.println("CHAT_BROADCAST: " + msg);
		
		for (ClientHandler clients : connectedClients) {
			
			if (!isSystemMsg && clients.equals(this)) {
				
				continue;
			
			} else {
			
				if (isSystemMsg) {
					
					clients.getDos().writeInt(Constants.CHAT_BROADCAST);
					clients.getDos().writeUTF(msg);
					
				} else {
					
					//once client add others to black list, they cannot receive and send messages
					if (clients.getUserBlackList().contains(this.getUsername()) || this.getUserBlackList().contains(clients.getUsername())) {
					
						continue;
					
					} else {
					
						clients.getDos().writeInt(Constants.CHAT_BROADCAST);
						clients.getDos().writeUTF(msg + Constants.PUBLIC_MESG_SUFFIX);
					}
				}
				
				clients.getDos().flush();
			}
		}
	}

	/*
	 * broadcast up-to-date client list to all other clients
	 */
	private void broadcastClientList() throws IOException {

		for (ClientHandler otherClient : connectedClients) {
			
			otherClient.getDos().writeInt(Constants.UPDATE_USERLIST);
			otherClient.getDos().writeUTF(getCurrentClientListString(otherClient));
			otherClient.getDos().flush();
		
		}
	}

	private String getCurrentClientListString(ClientHandler otherClient) {

		StringBuilder clientsList = new StringBuilder();
		
		if (connectedClients != null && connectedClients.size() > 0) {
		
			for (ClientHandler c : connectedClients) {
			
				//exclude the client who has been added in black list by the user
				if(c.getUserBlackList().contains(otherClient.getUsername())){
				
					continue;
				
				}else{
				
					clientsList.append(c.getUsername() + Constants.MSG_FIELD_SEPARATOR);
				
				}
			}
		}
		return clientsList.toString();
	}

	/*
	 * to judge whether a client login the current system or not
	 */
	private boolean isLogin(String data) {
		
		boolean flag = false;
	
		String username = getUsernameFromInfo(data);
		
		for (ClientHandler handler : connectedClients) {
		
			if (handler.getUsername() != null && handler.getUsername().equalsIgnoreCase(username)) {
			
				flag = true;
				break;
			
			}
		}
		
		return flag;
	}

	//check whether the name is exist in the database
	private boolean isExistUsername(String clientInfo) {
		
		boolean flag = true;

		String[] userInfoArray = getClientInfoArray(clientInfo);
		
		if (userInfoArray != null) {
		
			if (UserDBUtilities.loadByUserName(userInfoArray[0]) == null) {
			
				flag = false;
			
			}
		}

		System.err.println("isExitNickname = " + flag);
		return flag;
	}

	//check whether the user is registered user or not
	private boolean isRegisteredUser(String clientInfo) {
		
		boolean flag = false;
		
		String[] userInfoArray = getClientInfoArray(clientInfo);
		
		if (userInfoArray != null) {
		
			flag = UserDBUtilities.isRegisteredUser(userInfoArray[0], userInfoArray[1]);
		
		} else {
		
			flag = false;
		
		}
		
		return flag;
	}

	// to register client
	private boolean registerClient(String data) {
		
		boolean flag = false;
		
		String[] userInfoArray = getClientInfoArray(data);
		
		if (userInfoArray != null) {
		
			User user = new User();
			user.setUsername(userInfoArray[0]);
			user.setPassword(userInfoArray[1]);
			flag = UserDBUtilities.registerUser(user);
		
		}
		
		return flag;
	}

	//resolve name from message
	private String getUsernameFromInfo(String clientInfo) {

		return getClientInfoArray(clientInfo)[0];
	
	}

	private String[] getClientInfoArray(String clientInfo) {
		String[] clientInfoArray = null;

		if (clientInfo != null && !"".equals(clientInfo)) {
			
			clientInfoArray = clientInfo.split(Constants.MSG_FIELD_SEPARATOR);
		
		}

		return clientInfoArray;
	}

	//remove client from system
	private void removeClient() throws IOException {

		if (connectedClients != null && connectedClients.contains(this)) {

			connectedClients.remove(this);
		
		}
	}

	//resolve user array from message protocol
	private String[] getPrivateUsers(String data) {

		int start = data.indexOf(Constants.PRIVATE_USER_LIST_START);
		int length = Constants.PRIVATE_USER_LIST_START.length();
		int end = data.indexOf(Constants.PRIVATE_USER_LIST_END);
		
		String users = data.substring(start + length, end);
		
		return users.split(Constants.MSG_FIELD_SEPARATOR);
	}

	//resolve message part from message protocol
	private String getPrivateMsg(String data) {
		
		int start = data.indexOf(Constants.PRIVATE_USER_LIST_END);
		int length = Constants.PRIVATE_USER_LIST_END.length();
		
		return data.substring(start + length);
	}

	private void sendPrivateMsg(String data) throws IOException {
		
		String[] clientArray = getPrivateUsers(data);
		String msg = getPrivateMsg(data);

		ClientHandler handler = null;

		for (String u : clientArray) {
		
			handler = getClientHandler(u);
			
			if (handler != null) {
				
				//exclude client been in others' black list
				if (handler.getUserBlackList().contains(this.getUsername())) {
				
					continue;
				
				} else {
				
					handler.getDos().writeInt(Constants.CHAT_BROADCAST);
					handler.getDos().writeUTF(getUsername() + Constants.USERNAME_MESSAGE_SEPARATOR + msg + Constants.PRIVATE_MESG_SUFFIX);
					handler.getDos().flush();
				
				}
			}
		}
	}

	//returns instance of ClientHandler from ArrayList
	private ClientHandler getClientHandler(String clientName) {
		
		for (ClientHandler handler : connectedClients) {
		
			if (clientName.equalsIgnoreCase(handler.getUsername())) {
			
				return handler;
			
			}
		}
		
		return null;
	}

	//returns client's black list
	public List<String> getUserBlackList() {
	
		if (userBlackList == null) {
		
			userBlackList = new ArrayList<String>();
		
		}
		
		return userBlackList;
	}

	//save client's black list
	public void setUserBlackList(String userBlackListString) {
		
		String[] array = Utilities.resolveMsg(userBlackListString);

		if (array != null) {

			if (userBlackList == null) {
		
				userBlackList = new ArrayList<String>();
			
			}

			if (userBlackList.size() > 0) {
			
				userBlackList.clear();
			
			}

			for (String bl : array) {
			
				userBlackList.add(bl);
			
			}

		} else {

			userBlackList.clear();
		
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getClientIPAddress() {
		return clientIPAddress;
	}

	public void setClientIPAddress(String clientIPAddress) {
		this.clientIPAddress = clientIPAddress;
	}

	public String getClientSocketPort() {
		return clientSocketPort;
	}

	public void setClientSocketPort(String clientSocketPort) {
		this.clientSocketPort = clientSocketPort;
	}

	public String getClientFullPath() {
		return "[" + getClientIPAddress() + ":" + getClientSocketPort() + "]";
	}

	public DataOutputStream getDos() {
		return dos;
	}

	private void close() {
		stop = true;
		if (connectedClients != null && connectedClients.contains(this)) {
			connectedClients.remove(this);
		}

		try {
			if (dis != null)
				dis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (dos != null)
				dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (remoteClient != null) {
				remoteClient.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.err.println("socket on serverThread is closed: " + this.username);
	}
}
