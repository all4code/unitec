package server;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class UserDBUtilities {

	private static String tableName = "T_User";

	//connect to HSQL standalone database
	private static Connection getConnection() {
		
		String username;
		String password;
		String dbURL;
		
		Connection conn = null;
		
		Properties properties = null;
		
		try {
			
			//retrieve information from property file
			properties = new Properties();
			properties.load(UserDBUtilities.class.getClassLoader().getResourceAsStream("jdbc.properties"));
			
			username = properties.getProperty("username");
			password = properties.getProperty("password");
			dbURL = properties.getProperty("url");
			
			conn = DriverManager.getConnection(dbURL, username, password);
	
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return conn;

	}

	public static boolean isRegisteredUser(String username, String password) {
		
		Connection conn = getConnection();
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		int i = 0;
		boolean flag = false;
		
		try {
		
			sql = "select count(*)from " + tableName + " where Upper(username)=? and Upper(password)=?";
			stat = conn.prepareStatement(sql);
			stat.setString(1, username.toUpperCase());
			stat.setString(2, password.toUpperCase());
			rs = stat.executeQuery();

			while (rs.next()) {
			
				i += rs.getInt(1);
			
			}

			if (i >= 1) {
			
				flag = true;
			
			} else {
			
				flag = false;
			
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rs,stat,conn);
		}

		return flag;
	}

	public static User loadByUserName(String username) {
		
		Connection conn = getConnection();
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		User user = null;

		try {
		
			sql = "select * from " + tableName + " where Upper(username)=?";
			stat = conn.prepareStatement(sql);
			stat.setString(1, username.toUpperCase());
			rs = stat.executeQuery();

			while (rs.next()) {
			
				user = new User();
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
			
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rs,stat,conn);
		}

		return user;
	}

	public static boolean registerUser(User user) {

		Connection conn = getConnection();
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		boolean flag = false;
		int row = 0;

		try {
		
			if (isRegisteredUser(user.getUsername(), user.getPassword())) {
			
				return false;
			
			} else {

				sql = "insert into " + tableName + " (username, password) values (?, ?)";
				stat = conn.prepareStatement(sql);
				stat.setString(1, user.getUsername());
				stat.setString(2, user.getPassword());

				row = stat.executeUpdate();

				if (row == 1) {

					flag = true;
				
				} else {
			
					flag = false;
				
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rs,stat,conn);
		}

		return flag;
	}

	public static void listAllUsers() {
		
		Connection conn = getConnection();
		PreparedStatement stat = null;
		ResultSet rs = null;
		String sql = "";
		
		try {

			sql = "select * from " + tableName;
			stat = conn.prepareStatement(sql);
			rs = stat.executeQuery();

			while(rs.next()){
		
				System.out.println(rs.getString("username") + ", " + rs.getString("password"));
			
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rs,stat,conn);
		}
	}

	private static void close(ResultSet rs, PreparedStatement stat, Connection conn){
		
		close(rs);
		close(stat);
		close(conn);
	}
	private static void close(Connection con) {
		
		try {
		
			if (con != null)
			
				con.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void close(java.sql.PreparedStatement ps) {
		try {
		
			if (ps != null)
			
				ps.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void close(ResultSet rs) {
		try {
			
			if (rs != null)
			
				rs.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
