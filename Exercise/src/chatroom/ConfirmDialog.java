package client;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

import common.Utilities;

public class ConfirmDialog {
	
	private static final long serialVersionUID = 5131889784238687823L;
	private String savePath;
	private JDialog dialog;
	private JButton btnSave, btnSaveAs, btnCancel;
	
	public String confirmPath(JFrame f) {
		
		dialog = new JDialog(f,true);
//		dialog.setModal(true);
		dialog.setTitle("Receive File".toUpperCase());
		dialog.setSize(420, 150);
		// dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// int x = Utilities.getMiddleLocationX(400);
		// int y = Utilities.getMiddleLocationY(200);
		// dialog.setLocation(x, y);
		dialog.setLayout(null);
		Font font = Utilities.getDialogFont();

		JLabel label = new JLabel("Please choose a place to save file:");
		label.setBounds(10, 10, 300, 30);
		label.setFont(font);

		btnSave = new JButton("save".toUpperCase());
		btnSave.setBounds(10, 60, 120, 30);
		btnSave.setFont(font);

		btnSaveAs = new JButton("save as".toUpperCase());
		btnSaveAs.setBounds(140, 60, 120, 30);
		btnSaveAs.setFont(font);

		btnCancel = new JButton("cancel".toUpperCase());
		btnCancel.setBounds(270, 60, 120, 30);
		btnCancel.setFont(font);

		dialog.add(label);
		
		btnSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				savePath = Utilities.getDefaultUploadFolder();
				ConfirmDialog.this.dialog.dispose();
			}

		});
		
		btnSaveAs.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = chooser.showOpenDialog(dialog);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					if (file.isDirectory()) {
						savePath = file.getAbsolutePath();
						ConfirmDialog.this.dialog.dispose();
					}
				}
			}

		});
		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				savePath = "";
				ConfirmDialog.this.dialog.dispose();
			}
		});

//		System.out.println("save path: " + savePath);
		
		dialog.add(btnSave);
		dialog.add(btnSaveAs);
		dialog.add(btnCancel);

		dialog.setVisible(true);

		return savePath;
	}
}
