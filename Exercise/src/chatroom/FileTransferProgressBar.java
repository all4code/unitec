package common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class FileTransferProgressBar{
	JFrame frame = null;
	static JProgressBar progressbar;
	JLabel label;
	Timer timer;
	JButton b;
	public FileTransferProgressBar() {
		frame = new JFrame("File Transfering".toUpperCase());
		frame.setBounds(100, 100, 400, 120);
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		Container contentPanel = frame.getContentPane();
		
		progressbar = new JProgressBar();
		progressbar.setOrientation(JProgressBar.HORIZONTAL);
		progressbar.setMinimum(0);
		progressbar.setMaximum(100);
		progressbar.setValue(0);
		progressbar.setStringPainted(true);
		progressbar.setPreferredSize(new Dimension(300, 30));
		progressbar.setBorderPainted(true);
		progressbar.setBackground(Color.pink);
		progressbar.setString("file transfer");
//		JPanel panel = new JPanel();
//		b = new JButton("运行");
//		b.setForeground(Color.blue);
////		b.addActionListener(this);
//		panel.add(b);
//		timer = new Timer(100, this);
//		contentPanel.add(panel, BorderLayout.NORTH);
//		contentPanel.add(label, BorderLayout.CENTER);
		contentPanel.add(progressbar, BorderLayout.CENTER);
		// frame.pack();
		frame.setVisible(true);
	}
//	public void actionPerformed(ActionEvent e) {
//		if (e.getSource() == b) {
//			timer.start();
//		}
//		if (e.getSource() == timer) {
//			int value = progressbar.getValue();
//			if (value < 100)
//				progressbar.setValue(++value);
//			else {
//				timer.stop();
//				frame.dispose();
//			}
//		}
//	}
//	public void stateChanged(ChangeEvent e1) {
//		int value = progressbar.getValue();
//		if (e1.getSource() == progressbar) {
//			label.setText("目前已完成进度：" + Integer.toString(value) + "%");
//			label.setForeground(Color.blue);
//		}
//	}
	
	
	public void setValue(int n){
		this.progressbar.setValue(n);
	}
	
	public int getValue(){
		return this.progressbar.getValue();
	}
	
	public void dispose(){
		this.frame.dispose();
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//			UIManager.setLookAndFeel("");
		} catch (Exception e) {
			Logger.getLogger(FileTransferProgressBar.class.getName()).log(Level.FINE,
			e.getMessage());
			e.printStackTrace();
		}
		FileTransferProgressBar b = new FileTransferProgressBar();
		for(int i = 1; i <= 10; i++){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(b.getValue()<100)
				b.setValue(i*10);
		}
		try {
			Thread.sleep(2000);
			progressbar.setValue(-1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
