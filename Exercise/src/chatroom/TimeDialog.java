import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class TimeDialog {
    private JLabel label = new JLabel();  
    private JButton confirm,cancel;   
    private JDialog dialog = null;  
    String result = "";  
    public String  showDialog(JFrame father) 
    {  

        label.setText("nothing");  
        label.setBounds(80,6,200,20);  
        confirm = new JButton("接受");  
        confirm.setBounds(100,40,60,20);  
        
        confirm.addActionListener(new ActionListener() {              
            @Override  
            public void actionPerformed(ActionEvent e) {  
                result = "000";  
                TimeDialog.this.dialog.dispose();  
            }  
        });  
        
        cancel = new JButton("拒绝");  
        cancel.setBounds(190,40,60,20);  
        cancel.addActionListener(new ActionListener() {  
              
            @Override  
            public void actionPerformed(ActionEvent e) {  
                result = "111";  
                TimeDialog.this.dialog.dispose();  
            }  
        });  
        
        dialog = new JDialog(father, true);  
        dialog.setLayout(null);  
        dialog.add(label);  
        dialog.add(confirm);  
        dialog.add(cancel);  
        
        dialog.pack();  
        dialog.setSize(new Dimension(350,100));  
        dialog.setLocationRelativeTo(father);  
        dialog.setVisible(true);  
        return result;  
        
    }

}