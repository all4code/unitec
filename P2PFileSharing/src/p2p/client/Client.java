package p2p.client;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class Client extends JFrame implements ActionListener, Runnable {

	private static final long serialVersionUID = 980389841528802556L;

	// define the user interface components
	private JButton jbtnConnect, jbtnFileSearch, jbtnDownload, jbtnDirChooser, jbtnUp, jbtnShare, jbtnDelete;
	private JPanel leftPanel, middlePanel, rightPanel;
	private JTextField jtfFileSearcher;
	private JTable jtbSearchResults, jtbClientList, jtbSharedFiles, jtbLocalFiles;
	private JScrollPane jspSearchResult, jspClientList, jspSharedFiles, jspLocalFiles;
	private JFileChooser jfChooser;
	private JLabel jlbConnectStatus, jlbDownloadFilename, jlbSharedFileList, jlbDirChooser;

	private String clientIpAddress;

	// define initial message in text fields
	private String fileChosserInitString = "Please select files to share";
	private String fileSearcherInitString = "Please type a file name";
	
	/*
	 * it is download port, there are three port can be used during testing they
	 * are DOWNLOAD_PORT_1, DOWNLOAD_PORT_2, DOWNLOAD_PORT_3 it should be change
	 * when test the system in one computer
	 */
	private static String downloadPort = ClientConstants.DOWNLOAD_PORT_2;

	// a flag can stop the thread
	private boolean runThread = true;
	
	private static boolean[] resultArray = null;

	// determine exit normally or accidently
	private static final boolean NORMAL = true;
	private static final boolean ERROR = false;

	// to store a reference of a file wanted to download
	private File file;

	// to store the references of shared files of this client
	private List<File> sharedFileArrayList;

	/*
	 * to store search result, the Key is filename, the Value is a List<String>
	 * stored the client IP address and download port
	 */
	private Map<String, List<String>> fileSearchRusultMap;

	// define the socket and IO streams
	private Socket client;
	private DataInputStream dis;
	private DataOutputStream dos;

	public Client() {

		// define a thread to take care of messages sent from the server
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setBounds(100, 100, 1210, 440);
		this.setResizable(false);
		this.setLayout(null);

		// one of three panels, it is the very left one to show connect button,
		// search result as well as download resources
		leftPanel = new JPanel();
		leftPanel.setBounds(0, 0, 400, 400);
		leftPanel.setLayout(null);
		leftPanel.setBorder(BorderFactory.createLoweredSoftBevelBorder());

		// connect button
		jbtnConnect = new JButton("Connect");
		jbtnConnect.setBounds(5, 5, 100, 25);
		jbtnConnect.addActionListener(this);

		// a label to show the status of client, either connected or
		// disconnected, also show a message when some error occur.
		jlbConnectStatus = new JLabel();
		jlbConnectStatus.setLayout(null);
		jlbConnectStatus.setBounds(110, 5, 285, 25);

		// type keyword here to search shared files
		jtfFileSearcher = new JTextField(300);
		jtfFileSearcher.setBounds(5, 35, 290, 25);
		jtfFileSearcher.setBorder(BorderFactory.createLoweredBevelBorder());
		jtfFileSearcher.addActionListener(this);

		// to listen when the mouse clicks the text field to replace the initial
		// message
		jtfFileSearcher.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (jtfFileSearcher.getText().equalsIgnoreCase(fileSearcherInitString)) {
					jtfFileSearcher.setText(null);
				}
			}

		});

		// a button to search shared files
		jbtnFileSearch = new JButton("Search");
		jbtnFileSearch.setBounds(300, 35, 95, 25);
		jbtnFileSearch.addActionListener(this);

		// Search Result
		jspSearchResult = new JScrollPane();
		jspSearchResult.setBounds(5, 65, 390, 155);

		jtbSearchResults = new JTable();
		jtbSearchResults.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jtbSearchResults.setLayout(null);
		jtbSearchResults.setBounds(0, 0, 390, 155);

		// when the mouse clicks a row of search result table, the resources
		// (clients) will appear in the bottom
		jtbSearchResults.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				String filename = null;
				int selectedRowNumber = jtbSearchResults.getSelectedRow();
				if (selectedRowNumber == -1) {
					filename = (String) jtbSearchResults.getValueAt(0, 0);
				} else {
					filename = (String) jtbSearchResults.getValueAt(selectedRowNumber, 0);
				}
				jlbDownloadFilename.setText(filename);
				jlbDownloadFilename.setVisible(true);
				jbtnDownload.setVisible(true);
				setClientListTable(fileSearchRusultMap.get(filename));
			}
		});

		// to build the search result table using P2PTableModel, which inherited
		// from DefaultTableModel
		buildTable(jtbSearchResults, getSearchResultTableColNames());
		jspSearchResult.setViewportView(jtbSearchResults);

		// download file label and download button
		jlbDownloadFilename = new JLabel("");
		jlbDownloadFilename.setLayout(null);
		jlbDownloadFilename.setBorder(BorderFactory.createLoweredBevelBorder());
		jlbDownloadFilename.setBounds(5, 225, 290, 25);

		jbtnDownload = new JButton("Download");
		jbtnDownload.setBounds(300, 225, 95, 25);
		jbtnDownload.addActionListener(this);

		// a list of clients that hold the files shared
		jspClientList = new JScrollPane();
		jspClientList.setBounds(5, 255, 390, 135);

		jtbClientList = new JTable();
		jtbClientList.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jtbClientList.setLayout(null);
		jtbClientList.setBounds(0, 0, 390, 135);

		// to build the client list table using P2PTableModel, which inherited
		// from DefaultTableModel
		buildTable(jtbClientList, getClientListTableColNames());

		jspClientList.setViewportView(jtbClientList);

		leftPanel.add(jbtnConnect);
		leftPanel.add(jtfFileSearcher);
		leftPanel.add(jbtnFileSearch);
		leftPanel.add(jlbConnectStatus);
		leftPanel.add(jlbDownloadFilename);
		leftPanel.add(jbtnDownload);
		leftPanel.add(jspSearchResult);
		leftPanel.add(jspClientList);
		// end of middle panel

		// a panel in the middle to show shared file in this client
		middlePanel = new JPanel();
		middlePanel.setBounds(400, 0, 400, 400);
		middlePanel.setLayout(null);
		middlePanel.setBorder(BorderFactory.createLoweredSoftBevelBorder());

		// just a title
		jlbSharedFileList = new JLabel("Shared File List:");
		jlbSharedFileList.setLayout(null);
		jlbSharedFileList.setBounds(5, 5, 300, 25);

		// a button to delete shared file or files
		jbtnDelete = new JButton("Delete");
		jbtnDelete.setLayout(null);
		jbtnDelete.setBounds(315, 5, 80, 25);
		jbtnDelete.addActionListener(this);

		// a table, located in the middle, to show shared files
		jspSharedFiles = new JScrollPane();
		jspSharedFiles.setBounds(5, 35, 390, 360);

		jtbSharedFiles = new JTable();
		jtbSharedFiles.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jtbSharedFiles.setLayout(null);
		jtbSharedFiles.setBounds(0, 0, 390, 335);

		// to listen when double click a row of shared table, the file in this
		// shared list (or table) will be deleted
		jtbSharedFiles.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int action = JOptionPane.showConfirmDialog(middlePanel, "do you want cancel the file sharing?", "Warning", JOptionPane.YES_NO_OPTION);
					if (action == JOptionPane.YES_OPTION) {
						int[] rows = jtbSharedFiles.getSelectedRows();
						removeSharedRows(rows);
					}
				}
			}
		});

		// to build the shared files table using P2PTableModel, which inherited
		// from DefaultTableModel
		buildTable(jtbSharedFiles, getSharedFilesTableColNames());
		jspSharedFiles.setViewportView(jtbSharedFiles);

		middlePanel.add(jlbSharedFileList);
		middlePanel.add(jbtnDelete);
		middlePanel.add(jspSharedFiles);
		// end of middle panel

		// a panel in the right, to help choosing files from local computer
		rightPanel = new JPanel();
		rightPanel.setBounds(800, 0, 400, 400);
		rightPanel.setLayout(null);
		rightPanel.setBorder(BorderFactory.createLoweredSoftBevelBorder());

		// a label to show currently selected file path
		jlbDirChooser = new JLabel();
		jlbDirChooser.setOpaque(true);
		jlbDirChooser.setBackground(Color.white);
		jlbDirChooser.setBorder(BorderFactory.createLoweredBevelBorder());
		jlbDirChooser.setBounds(5, 5, 390, 25);

		// a button to activate file chooser
		jbtnDirChooser = new JButton("Broswe");
		jbtnDirChooser.setBounds(5, 35, 80, 25);
		jbtnDirChooser.addActionListener(this);

		// a button to show the parent path
		jbtnUp = new JButton("UP");
		jbtnUp.setBounds(230, 35, 80, 25);
		jbtnUp.addActionListener(this);

		// a button to share a file or files
		jbtnShare = new JButton("Share");
		jbtnShare.setBounds(315, 35, 80, 25);
		jbtnShare.addActionListener(this);

		// local files table on the right
		jspLocalFiles = new JScrollPane();
		jspLocalFiles.setBounds(5, 60, 390, 335);

		jtbLocalFiles = new JTable();
		jtbLocalFiles.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jtbLocalFiles.setLayout(null);
		jtbLocalFiles.setBounds(0, 0, 390, 335);

		// to listen when double click a row, it will be shared, if a directory,
		// will be show its sub directory
		jtbLocalFiles.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					String fileName = (String) jtbLocalFiles.getValueAt(jtbLocalFiles.getSelectedRow(), 0);
					file = new File(getFileFullPath(fileName));
					if (file.isDirectory()) {
						jlbDirChooser.setText(file.getPath());
						setLocalTable();
					} else {
						sharedFileArrayList = new ArrayList<File>();
						sharedFileArrayList.add(file);
						setSharedTable();
					}
				}
			}
		});

		// to build the local file table using P2PTableModel, which inherited
		// from DefaultTableModel
		buildTable(jtbLocalFiles, getLocalFileTableColNames());
		jspLocalFiles.setViewportView(jtbLocalFiles);

		rightPanel.add(jlbDirChooser);
		rightPanel.add(jbtnDirChooser);
		rightPanel.add(jbtnUp);
		rightPanel.add(jbtnShare);
		rightPanel.add(jspLocalFiles);
		// end of right panel

		this.getContentPane().add(leftPanel);
		this.getContentPane().add(middlePanel);
		this.getContentPane().add(rightPanel);

		// to listen when window closing, the system will exit
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent event) {
				try {
					if (getDos() != null) {
						getDos().writeInt(ClientConstants.CLIENT_LEAVE);
						getDos().flush();
					}
					close(NORMAL);
				} catch (SocketException e) {
					System.out.println(e.getMessage());
					close(ERROR);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		});

		// initialise status of buttons
		initButtonStatus();
	}

	/*
	 * to build and format the tables, 'table' is a reference of JTable,
	 * 'colnames' is a reference of List<String> which stores the the headers of
	 * table columns
	 */
	private void buildTable(JTable table, Vector<String> colnames) {
		table.setModel(setTableModel(colnames));
		table.getColumnModel().getColumn(0).setPreferredWidth(300);
		table.getColumnModel().getColumn(1).setPreferredWidth(87);
	}

	// to build tables using P2PTableModel, which inherited from
	// DefaultTableModel
	private TableModel setTableModel(Vector<String> tableColNames) {
		return new P2PTableModel(new Vector<Vector<String>>(), tableColNames);
	}

	// return a Vector<String> stored column names of search result table
	private Vector<String> getSearchResultTableColNames() {
		Vector<String> colnames = new Vector<String>();
		colnames.add("Name");
		colnames.add("Source");
		return colnames;
	}

	// return a Vector<String> stored column names of shared files table
	private Vector<String> getSharedFilesTableColNames() {
		Vector<String> colnames = new Vector<String>();
		colnames.add("Name");
		colnames.add("Size");
		return colnames;
	}

	// return a Vector<String> stored column names of client list table
	private Vector<String> getClientListTableColNames() {
		Vector<String> colnames = new Vector<String>();
		colnames.add("Name");
		colnames.add("Status");
		return colnames;
	}

	// return a Vector<String> stored column names of local file table
	private Vector<String> getLocalFileTableColNames() {
		Vector<String> colnames = new Vector<String>();
		colnames.add("Name");
		colnames.add("Size");
		return colnames;
	}

	// to change the status of buttons when click disconnect button
	private synchronized void offlineStatus() {
//		System.out.println(Thread.currentThread().getName() + " come in");
		jlbDownloadFilename.setText("");
		jlbDownloadFilename.setVisible(false);
		jbtnDownload.setVisible(false);
//		jtfFileSearcher.setText(null);
		jtfFileSearcher.setText(fileSearcherInitString);
		jbtnFileSearch.setEnabled(false);
		jspSearchResult.setVisible(false);
		if(fileSearchRusultMap != null){
			fileSearchRusultMap.clear();
		}
		
		jspClientList.setVisible(false);
		
		jlbConnectStatus.setText("Disconnected");
		jlbConnectStatus.setForeground(Color.RED);
		jbtnConnect.setText("Connect");
		
		setTitle("");
		
		if(fileSearchRusultMap != null && fileSearchRusultMap.size() > 0){
			fileSearchRusultMap.clear();
		}
//		System.out.println(Thread.currentThread().getName() + " go away");
	}

	// to initialise the status of buttons
	private void initButtonStatus() {
		offlineStatus();
		jbtnDelete.setVisible(false);
		jbtnDelete.setEnabled(false);
		jspSharedFiles.setVisible(false);
		jtfFileSearcher.setText(fileSearcherInitString);
		jlbDirChooser.setText(fileChosserInitString);
		jbtnUp.setEnabled(false);
		jbtnUp.setVisible(false);
		jbtnShare.setEnabled(false);
		jbtnShare.setVisible(false);
		jspLocalFiles.setVisible(false);
	}

	// to do different task when buttons are clicked
	@Override
	public void actionPerformed(ActionEvent event) {

		Object obj = event.getSource();

		// to connect or disconnect
		if (obj == jbtnConnect) {

			if (jbtnConnect.getText().equalsIgnoreCase("connect")) {
				connectToServer();
			} else {
				disConnectToServer();
			}

		}
		// to search files that other clients shared
		else if (obj == jbtnFileSearch || obj == jtfFileSearcher) {

			String keyWords = jtfFileSearcher.getText();
			if (keyWords == null || "".equalsIgnoreCase(keyWords) || fileSearcherInitString.equalsIgnoreCase(keyWords)) {
				JOptionPane.showMessageDialog(leftPanel, "Please enter the filename you want looking for!");
				return;
			}
			findMatchedFile(keyWords);

		}
		// to download selected file
		else if (obj == jbtnDownload) {
			
			String filename = jlbDownloadFilename.getText();
			List<String> clients = findClientListByFilename(filename);
//			int num = clients.size();
			//lock download button and search result table during downloading
			jbtnDownload.setEnabled(false);
			jtbSearchResults.setEnabled(false);
			
			sendDownloadRequestToClient(filename, clients);
		}

		// to cancel a file or files shared
		else if (obj == jbtnDelete) {
			int[] rows = jtbSharedFiles.getSelectedRows();
			if (rows.length <= 0) {
				JOptionPane.showMessageDialog(middlePanel, "Please select at least one file to delete!");
				return;
			} else {
				removeSharedRows(rows);
			}

		}
		// to show the parent path
		else if (obj == jbtnUp) {

			if (file.getParentFile() != null) {
				jbtnUp.setEnabled(true);
				file = file.getParentFile();
				if (file.isDirectory()) {
					jlbDirChooser.setText(file.getPath());
					setLocalTable();
				}
			} else {
				jbtnUp.setEnabled(false);
			}

		}
		// to share a file or files
		else if (obj == jbtnShare) {
			int[] rows = jtbLocalFiles.getSelectedRows();
			if (rows.length <= 0) {
				JOptionPane.showMessageDialog(this, "Please select at least one file to share");
				return;
			} else {
				getValidatedFileArray(jtbLocalFiles, rows);
				setSharedTable();
				enableDeleteButton();
			}
		}
		// to activate file chooser
		else if (obj == jbtnDirChooser) {
			String curFilePath = jlbDirChooser.getText();

			// set the default path to project folder
			if (curFilePath == null || "".equals(curFilePath) || fileChosserInitString.equals(curFilePath)) {
				curFilePath = System.getProperty("user.dir");
			}

			jfChooser = new JFileChooser(curFilePath);
			jfChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			
			int returnVal = jfChooser.showOpenDialog(this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				file = jfChooser.getSelectedFile();
				if (file.isDirectory()) {
					jlbDirChooser.setText(file.getPath());
					setLocalTable();
					jbtnShare.setEnabled(true);
				}
			}
		}
	}

	// to return client list with given filename
	private List<String> findClientListByFilename(String filename) {
		List<String> clientList = null;
		if (fileSearchRusultMap.containsKey(filename)) {
			clientList = fileSearchRusultMap.get(filename);
		}
		return clientList;
	}

	// process messages from the server
	@Override
	public void run() {
		try {
			while (runThread) {
				if (!runThread) {
					break;
				}
				// receive a message from the server, determine message
				int messageType = dis.readInt();
				String data = null;
				System.out.println("Client: " + messageType);

				// decode message and process
				switch (messageType) {
				case ClientConstants.CLIENT_SEARCH:
					System.out.println("CLIENT_SEARCH");
					// exclude the files that this client shared
					data = dis.readUTF();
					resoveSearchResults(data);
					if (fileSearchRusultMap != null) {
						setSearchResultTable();
					}
					break;
				case ClientConstants.SERVER_PING:
					System.out.println("SERVER_PING");
					// to respond a ping from server
					getDos().writeInt(ClientConstants.CLIENT_PING);
					getDos().writeUTF("Client is online!");
					getDos().flush();
					break;
				case ClientConstants.CLIENT_LEAVE:
					System.out.println("CLIENT_LEAVE");
					data = dis.readUTF();
					if (data != null) {
						System.out.println("The client [" + data + "] left!");
					}
					// delete left client from search result table
					deleteSearchResultByClient(data);
					break;
				}
			}
		} catch (SocketException e) {
			close(ERROR);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// delete left client from search result table
	private void deleteSearchResultByClient(String data) {
		if (fileSearchRusultMap != null && fileSearchRusultMap.size() > 0) {
			
			int action = JOptionPane.showConfirmDialog(this, "Some clients left, do you want refresh the search result?");
			
			if (action == JOptionPane.OK_OPTION) {
				Set<String> keys = fileSearchRusultMap.keySet();
				List<String> value = null;
				List<String> tempKeysToBeRemove = new ArrayList<String>();

				// delete the client from HashMap
				for (String filename : keys) {
					
					//return a List<String> of clients who have shared this file
					value = fileSearchRusultMap.get(filename);
					
					//looking for whether the left client is in the client list
					Iterator<String> it = value.iterator();
					
					while (it.hasNext()) {
						if (it.next().equalsIgnoreCase(data.trim())) {
							it.remove();
						}
					}

					//if a file only shared by the left client, the file will be stored in a temporary List<String>
					if (value.size() <= 0) {
						tempKeysToBeRemove.add(filename);
					}
				}

				// to delete the files that only be shared by the left client
				for (String filename : tempKeysToBeRemove) {
					fileSearchRusultMap.remove(filename);
				}
				
				// to refresh the search result table
				setSearchResultTable();
			}
		}
	}

	// attempt to connect to the defined remote host
	private void connectToServer() {
		
		try {
			client = new Socket(ClientConstants.SERVER_IP_ADDRESS, ClientConstants.SERVER_PORT);
			dis = new DataInputStream(client.getInputStream());
			dos = new DataOutputStream(client.getOutputStream());

			getDos().writeInt(ClientConstants.CLIENT_JOIN);
			// send client download port
			getDos().writeUTF(downloadPort);
			getDos().flush();

			clientIpAddress = dis.readUTF();
			if (clientIpAddress != null && !"".equalsIgnoreCase(clientIpAddress)) {
				successToConnect();
			} else {
				failToConnect();
				return;
			}

			//if there are shared files in this client, the filename will transfer to the central server
			if (SharedFileRepository.getsharedFileRepository().size() > 0) {
				updateSharedFileListToServer();
			}

			// define a thread to take care of messages sent from the server
			runThread = true;
			Thread t = new Thread(this);
			t.start();

		} catch (SocketException e) {
			failToConnect();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// disconnect to server
	private void disConnectToServer() {
		try {
			getDos().writeInt(ClientConstants.CLIENT_LEAVE); // determine the
			getDos().flush();
			close(NORMAL);
		} catch (SocketException e) {
			close(ERROR);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// set the status of components when connect with server
	private void successToConnect() {
		jbtnConnect.setText("Disconnect");
		jlbConnectStatus.setText("Connected");
		jlbConnectStatus.setForeground(new Color(0, 120, 0));
		jbtnFileSearch.setEnabled(true);
		
		setTitle(clientIpAddress);
	}

	// set the status of components when fail to connect with server
	private void failToConnect() {
		JOptionPane.showMessageDialog(this, "There are errors when connects to server.");
		setTitle("");
		close(ERROR);
	}

	// send message to server in order to looking for files with a given keyword
	private void findMatchedFile(String keyWords) {
		try {
			getDos().writeInt(ClientConstants.CLIENT_SEARCH);
			getDos().writeUTF(keyWords);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * resolve the customised protocol to obtain search result 
	 * the protocol looks like for example:
	 * 
	 * HelloWord.java&=&192.168.1.1:5000&=&192.168.2.2:9000@=@Test.java&=&10.22.0.1:10000
	 * 
	 * @=@ is REPOSITORY_ITEM_SEPARATOR
	 * &=& is REPOSITORY_FIELD_SEPARATOR
	 * 
	 */
	private void resoveSearchResults(String data) {
		if (data == null || "".equalsIgnoreCase(data.trim())) {
			JOptionPane.showMessageDialog(leftPanel, "no files can be matched!");
		} else {
			//resolve each file item in the protocol
			String[] files = data.split(ClientConstants.REPOSITORY_ITEM_SEPARATOR);
			fileSearchRusultMap = new HashMap<String, List<String>>();
			List<String> clients = null;
			for (String str : files) {
				
				//resolve each field of each file item in the protocol
				String[] temp = str.split(ClientConstants.REPOSITORY_FIELD_SEPARATOR);
				
				clients = new ArrayList<String>();
				
				// Obtain the clients as values of HashMap
				for (int i = 1; i < temp.length; i++) {
					clients.add(temp[i]);
				}
				
				// Obtain filename as the key of the HashMap 
				fileSearchRusultMap.put(temp[0], clients);
			}
			
			//print resolved protocol on Console
			Set<String> keys = fileSearchRusultMap.keySet();
			for (String key : keys) {
				System.out.println("Client:resovelSearchResults: " + key + ":::" + fileSearchRusultMap.get(key).size());
				List<String> k = fileSearchRusultMap.get(key);
				for (String s : k) {
					System.out.println(s + ",");
				}
			}
		}
	}

	//when delete button is clicked
	private void removeSharedRows(int[] rows) {
		DefaultTableModel model = (DefaultTableModel) jtbSharedFiles.getModel();
		for (int i = rows.length; i > 0; i--) {
			String filename = ((String) model.getValueAt(rows[i - 1], 0));
			SharedFileRepository.getsharedFileRepository().remove(filename);
			model.removeRow(rows[i - 1]);
		}
		SharedFileRepository.getsharedFileRepository().printRepository();
		disableDeleteButton();
		// update shared file list and send to server
		updateSharedFileListToServer();
	}

	private void disableDeleteButton() {
		if (jtbSharedFiles.getRowCount() <= 0) {
			jbtnDelete.setEnabled(false);
		}
	}

	private void enableDeleteButton() {
		if (jtbSharedFiles.getRowCount() > 0) {
			jbtnDelete.setEnabled(true);
		}
	}

	// judge whether the selected are all normal file or included folders.
	private void getValidatedFileArray(JTable table, int[] rows) {
		boolean isAllNormalFile = true;
		sharedFileArrayList = new ArrayList<File>();
		for (int i = 0; i < rows.length; i++) {
			file = new File(getFileFullPath((String) table.getValueAt(rows[i], 0)));
			if (file.isFile()) {
				sharedFileArrayList.add(file);
			} else {
				isAllNormalFile = false;
			}
		}
		if (!isAllNormalFile) {
			JOptionPane.showMessageDialog(rightPanel, "The directories that you selected are ignored");
		}
	}

	private String getFileFullPath(String filename) {
		return jlbDirChooser.getText() + File.separator + filename;
	}

	private void setLocalTable() {

		File[] files = file.listFiles();
		// get table model
		DefaultTableModel model = (DefaultTableModel) jtbLocalFiles.getModel();
		model.setRowCount(0);
		for (File file : files) {
			String secondParm = null;
			if (file.isDirectory()) {
				secondParm = "<DIR>";
			} else {
				secondParm = String.valueOf(file.length());
			}
			Object[] row = { file.getName(), secondParm };
			model.addRow(row);
		}

		jspLocalFiles.setVisible(true);
		jbtnUp.setVisible(true);
		jbtnShare.setVisible(true);

		if (file.getParentFile() != null) {
			jbtnUp.setEnabled(true);
		} else {
			jbtnUp.setEnabled(false);
		}

	}

	private void setSearchResultTable() {
		// get table model
		DefaultTableModel model = (DefaultTableModel) jtbSearchResults.getModel();
		model.getDataVector().clear();

		if (fileSearchRusultMap != null && fileSearchRusultMap.size() > 0) {
			Vector<String> v = null;
			String firstFilename = null;
			Set<String> fileSearchSet = fileSearchRusultMap.keySet();
			for (String f : fileSearchSet) {
				if (firstFilename == null || "".equals(firstFilename)) {
					firstFilename = f;
				}
				v = new Vector<String>();
				v.add(f);
				v.add(String.valueOf(fileSearchRusultMap.get(f).size()));
				model.addRow(v);
			}
			jspSearchResult.setVisible(true);
			setClientListTable(fileSearchRusultMap.get(firstFilename));
		} else {
			jspSearchResult.setVisible(false);
			buildTable(jtbSearchResults, getSearchResultTableColNames());
			jlbDownloadFilename.setText("");
			jlbDownloadFilename.setVisible(false);
			jbtnDownload.setVisible(false);
			// jtbSearchResults.setModel(setTableModel(getSearchResultTableColNames()));
			setClientListTable(null);
		}
	}

	private void setClientListTable(List<String> clients) {
		DefaultTableModel model = (DefaultTableModel) jtbClientList.getModel();
		model.getDataVector().clear();
		Vector<String> v = null;

		if (clients != null) {
			for (String c : clients) {
				v = new Vector<String>();
				v.add(c);
				v.add("");
				model.addRow(v);
			}

			jspClientList.setVisible(true);
		} else {
			jspClientList.setVisible(false);
			buildTable(jtbClientList, getClientListTableColNames());
		}
	}

	private void setSharedTable() {
		// get table model
		DefaultTableModel model = (DefaultTableModel) jtbSharedFiles.getModel();
		
		// define duplicate files occur
		boolean flag = false;
		
		Vector<String> v = null;

		for (File f : sharedFileArrayList) {
			v = new Vector<String>();
			v.add(f.getName());
			v.add(String.valueOf(f.length()));
			System.out.println(f.getName() + "," + f.length());

			if (SharedFileRepository.getsharedFileRepository().containsKey(f.getName())) {
				flag = true;
				continue;
			} else {
				model.addRow(v);
				SharedFileRepository.getsharedFileRepository().put(f.getName(), f);
			}
		}
		
		if(flag){
			JOptionPane.showMessageDialog(rightPanel, "Duplicated files are ignored!");
		}
		
		jspSharedFiles.setVisible(true);
		jbtnDelete.setVisible(true);

		// update shared file list to server
		updateSharedFileListToServer();
	}

	private void updateSharedFileListToServer() {
		if (getDos() != null) {
			// update shared file list to server
			StringBuilder str = new StringBuilder();
			Set<String> s = SharedFileRepository.getsharedFileRepository().keySet();
			for (String filename : s) {
				str.append(filename + ",");
			}

			String strFilenames = str.toString();
			try {
				if (getDos() != null) {
					getDos().writeInt(ClientConstants.CLIENT_PUBLISH);
					getDos().writeUTF(strFilenames);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void close(boolean isNormal) {
		
		runThread = false;
		
		if (!isNormal) {
			if (client == null || client.isClosed()) {
				System.out.println("Client socket is closed!");
			} else {
				System.out.println("Server socket could be closed!");
				JOptionPane.showMessageDialog(this, "Server socket could be closed!", "Error", JOptionPane.ERROR_MESSAGE);
				jlbConnectStatus.setText("Disconnected!");
			}
		}
		try {
			if (dis != null)
				dis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (dos != null)
				dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (client != null)
				client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		offlineStatus();
	}

	//to invoke new threads to download a or a part of file from remote client 
	private void sendDownloadRequestToClient(String filename, List<String> clients) {
		// determine the number of threads
		int num = clients.size();
		
		// create a thread pool 
		ExecutorService threadPool = Executors.newFixedThreadPool(ClientConstants.THREAD_POOL_SIZE);
		
		// to return the results of sub threads
		resultArray = new boolean[num];
		
		try {
			for (int i = 0; i < clients.size(); i++) {
				String[] array = clients.get(i).split(":");
				/*
				 *       array[0]: IP address;
				 *       array[1]: download port;
				 *       filename: the file that intend to download
				 *  	        i: the No. of sub thread
				 *            num: the total number of sub threads
				 *  jtbClientList: client list table
				 *    resultArray: a boolean array to store the result of sub threads
				 */
				threadPool.execute(new DownloadClient(new Socket(array[0],Integer.valueOf(array[1])),filename,i,num,jtbClientList,resultArray));
			}
			
			// to wait all sub threads finish and show the final result
			new Thread(new Runnable(){
				@Override
				public void run() {
					
					//shutdown tasks
					threadPool.shutdown();
					
					while(!threadPool.isTerminated()){
						//do nothing just waiting
					}
					
					// if all sub threads finish successfully, the flag is true
					boolean flag = true;
					
					
					for(boolean b : resultArray){
						if(!b){
							flag = false;
							break;
						}
					}
					
					if(flag){
						jbtnDownload.setEnabled(true);
						jtbSearchResults.setEnabled(true);
						JOptionPane.showMessageDialog(null, "Download finish successfully!");
					}
					else{
						JOptionPane.showMessageDialog(null, "Download is failed!");
					}
				}
			}).start();
			
			
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public DataOutputStream getDos() {
		return dos;
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client c = new Client();
					c.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		try {
			new DownloadServerListener(downloadPort);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}