package p2p.client;

import java.awt.HeadlessException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.net.SocketException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class DownloadClient implements Runnable {

	private Socket client;
	private DataInputStream in;
	private DataOutputStream out;
	private DataInputStream fin;
	private DataOutputStream fout;

	private String ip, filePath;
	private int port, block, threadNum, threadCount;
	private long start, end, fileLength;
	
	private boolean stop = false;
	private boolean[] resultArray;
	
	private JTable jtbClientList;

	private RandomAccessFile racFile;

	public DownloadClient(Socket client, String filename, int threadNum, int threadCount, JTable jtbClientList, boolean[] resultArray) {
		try {
			
			this.client = client;
			this.resultArray = resultArray;
			this.jtbClientList = jtbClientList;
			
			// the total number of threads to download a file
			this.threadCount = threadCount;
			
			// the No. of thread
			this.threadNum = threadNum;

			ip = client.getInetAddress().getHostAddress();
			port = client.getPort();
			System.out.println("IP and Port in DownloadClient: " + ip + ":" + port);
			
			// the target default folder is "destination" in project folder
			filePath = System.getProperty("user.dir") + File.separator + "destination" + File.separator + filename;
			System.out.println("file path: " + new File(filePath).getAbsolutePath());

			in = new DataInputStream(client.getInputStream());
			out = new DataOutputStream(client.getOutputStream());

			// to ensure the file is available in target client
			out.writeInt(ClientConstants.REMOTE_FETCH);
			System.out.println("send REMOTE_FETCH");
			out.writeUTF(filename);
			out.flush();
			
			//initialise client list table
			init();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void init() {
		DefaultTableModel model = (DefaultTableModel) getTableModel(jtbClientList);
		Vector<Vector<String>> data = model.getDataVector();
		Iterator<Vector<String>> it = data.iterator();
		while (it.hasNext()) {
			it.next().set(1, "Connecting");
		}
		model.fireTableDataChanged();
	}

	//to show the percentage of downloading progress
	@SuppressWarnings("unchecked")
	private void updataClientListTableModel(String percenttage) {
		DefaultTableModel model = (DefaultTableModel) getTableModel(jtbClientList);
		Vector<Vector<String>> data = model.getDataVector();
		Iterator<Vector<String>> it = data.iterator();
		Vector<String> row = null;
		while (it.hasNext()) {
			row = it.next();
			if(row.get(0).equals(ip + ":" + port)){
				row.set(1, percenttage);
			}
		}
		model.fireTableDataChanged();
	}

	private TableModel getTableModel(JTable table) {
		return (DefaultTableModel) table.getModel();

	}

	@Override
	public void run() {

		while (!stop) {
			try {
				if (stop) {
					break;
				}
				int messageType = in.readInt();
				switch (messageType) {
				case ClientConstants.REMOTE_FETCH_ERROR:
					System.out.println("REMOTE_FETCH_ERROR");
					JOptionPane.showMessageDialog(null, "The file cannot be found in target client!");
					break;
				case ClientConstants.REMOTE_FETCH_SUCCESS:
					System.out.println("REMOTE_FETCH_SUCCESS");
					// to obtain file length
					fileLength = in.readLong();
					
					// specify a range of input stream
					block = Math.round(((float) fileLength + (float) threadCount - 1) / (float) threadCount);
					
					// download thread starts at the position of file
					start = block * threadNum;
					
					// download thread ends at the position of file
					end = block * (threadNum + 1) - 1;
					end = (end >= fileLength) ? (fileLength - 1) : end;
					System.out.println("I am thread " + threadNum + " i will download from " + start + " to " + end);
					
					out.writeInt(ClientConstants.REQUIRE_RANGE);
					out.writeUTF(start + ClientConstants.REPOSITORY_FIELD_SEPARATOR + end);
					out.flush();
					break;
				case ClientConstants.DOWNLOAD_START:
					System.out.println("DOWNLOAD_START");
					System.out.println("File length: " + fileLength);
					download();
					break;
				}
			} catch (HeadlessException e) {
				e.printStackTrace();
			} catch (SocketException e) {
				System.out.println("Connection stop!");
				close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	//download a part of file
	private void download() {

		int bufferSize = ClientConstants.CLIENT_BUFFER_SIZE;
		byte[] buf = new byte[bufferSize];
		int len = 0;
		long writtenLength = 0L;

		try {
			fin = new DataInputStream(client.getInputStream());
			racFile = new RandomAccessFile(new File(filePath), "rwd");
			
			if(racFile.length() != fileLength){
				racFile.setLength(fileLength);
			}
			
			//move to the start position
			racFile.seek(start);
			System.out.println("Thred: " + threadNum + " Download start at: " + start + " End: " + end );
			
			while ((len = fin.read(buf)) != -1) {
				writtenLength += len;
				racFile.write(buf, 0, len);

				//update the progress of downloading
				updataClientListTableModel(getPercent(writtenLength, (end - start + 1)));
			}
			
			System.out.println("Thred: " + threadNum + " finish!");
			System.out.println("Thred: " + threadNum + " WrittenLength: " + writtenLength + " Success: " + (writtenLength == (end - start + 1)));
			System.out.println("Thred: " + threadNum + " File saved at " + filePath + "\n");
			resultArray[threadNum]=true;
			
			close();
			
		}catch(SocketException e){
			System.out.println("The connection stop!");
			close();
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getPercent(long x, long y) {
		String percent = "";
		double dx = x * 1.0;
		double dy = y * 1.0;
		double by = dx / dy;
		DecimalFormat df = new DecimalFormat("##%");
		percent = df.format(by);
		return percent;
	}
	
	private void close() {
		stop = true;
		try {
			racFile.close();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			if (in != null) {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (fin != null) {
				fin.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (fout != null) {
				fout.close();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			if (client != null) {
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
