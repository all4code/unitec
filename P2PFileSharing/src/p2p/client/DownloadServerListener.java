package p2p.client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * As a file download Server, wait clients that wants connect with.
 * 
 * @author Po DONG
 *
 */
public class DownloadServerListener {
	private ServerSocket listener;
	private Socket client;

	private ArrayList<DownloadServerThread> clientList = null;
	private boolean stop;

	public DownloadServerListener(String downloadPort) {
		stop = false;
		clientList = new ArrayList<DownloadServerThread>();
		try {
			listener = new ServerSocket(Integer.valueOf(downloadPort));
			System.out.println("Download listener start at: " + listener.getInetAddress().getHostAddress()+ ":" + listener.getLocalPort());
			start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void start() {
		try {
			while (!stop) {
				if (stop) {
					close();
				}
				client = listener.accept();
				DownloadServerThread dst = new DownloadServerThread(client);
				dst.start();
				clientList.add(dst);
			}
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}
	
	private void close() {
		stop = true;
		try {
			if (listener != null) {
				listener.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
