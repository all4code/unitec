package p2p.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.net.SocketException;

/**
 * send a file or a part of file to other client who want to download
 * @author Po dong
 *
 */
public class DownloadServerThread extends Thread {
	private Socket client;
	private DataOutputStream out;
	private DataOutputStream fout;
	private DataInputStream in;
	private File file;
	private boolean stop = false;

	public DownloadServerThread(Socket client) {
		try {
			this.client = client;
			out = new DataOutputStream(client.getOutputStream());
			in = new DataInputStream(client.getInputStream());

		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (!stop) {
				if (stop) {
					break;
				}
				int mesageType = in.readInt();
				String data = null;
				switch (mesageType) {
				case ClientConstants.REMOTE_FETCH:
					System.out.println("REMOTE_FETCH");
					data = in.readUTF();
					
					//match the filename with local shared file repository
					if (data != null && !"".equalsIgnoreCase(data.trim())) {
						if (SharedFileRepository.getsharedFileRepository().containsKey(data)) {
							file = findFileFromRepository(data);
							if (file != null) {
								out.writeInt(ClientConstants.REMOTE_FETCH_SUCCESS);
								out.writeLong(file.length());
								out.flush();
							} else {
								out.writeInt(ClientConstants.REMOTE_FETCH_ERROR);
								out.flush();
							}
						}
					}
					break;
				case ClientConstants.REQUIRE_RANGE:
					data = in.readUTF();
					
					// obtain the position of start, end
					if (data != null && !"".equalsIgnoreCase(data.trim())) {
						String[] range = data.split(ClientConstants.REPOSITORY_FIELD_SEPARATOR);
						if(range != null && range.length > 0 ){
							long start = Long.valueOf(range[0]);
							long end = Long.valueOf(range[1]);
							// start provide file
							sendFile(start, end);
						}
					}
					break;
				}
			}
		} catch (SocketException e) {
			// e.printStackTrace();
			System.out.println("download server thread: download finish!");
			close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendFile(long start, long end) {
		try {
			
			//read file from local disk at the specified position
			RandomAccessFile racFile = new RandomAccessFile(file, "r");  
			racFile.seek(start);

            byte[] buff = new byte[ClientConstants.CLIENT_BUFFER_SIZE];

            fout = new DataOutputStream(client.getOutputStream());

			int len = 0;
			long writtenLength = 0L;
			long pointer = 0L;
			out.writeInt(ClientConstants.DOWNLOAD_START);
			
			// read file to system from 'start' to 'end' based on the number of resources and file length
			while ((len = racFile.read(buff)) != -1) {
				pointer = racFile.getFilePointer();
				if (pointer > end) {
					len = (int) (len - (pointer - end - 1 ));
					writtenLength += len;
					fout.write(buff, 0, len);
					break;
				}
				writtenLength += len;
				fout.write(buff, 0, len);
			}
			System.out.println("************Download server end have sent " + writtenLength + " ***************");
			
			fout.flush();
			racFile.close();
			fout.close();
			
		} catch (SocketException e){
			System.out.println("The connection is stop!");
			this.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private File findFileFromRepository(String data) {
		File file = null;
		if (SharedFileRepository.getsharedFileRepository().containsKey(data)) {
			file = SharedFileRepository.getsharedFileRepository().get(data);
		}
		return file;
	}

	private void close() {
		try {
			if (fout != null) {
				fout.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (in != null) {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (client != null) {
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
