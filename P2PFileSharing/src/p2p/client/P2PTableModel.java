package p2p.client;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

class P2PTableModel extends DefaultTableModel {
	private static final long serialVersionUID = -1162525122405163277L;
	boolean[] columnEditables = new boolean[] { false, false };
	
	public P2PTableModel(Vector<Vector<String>> rows, Vector<String> colnames) {
		super(rows, colnames);
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return columnEditables[column];
	}
}