package p2p.client;

import java.io.File;
import java.util.HashMap;
import java.util.Set;

public class SharedFileRepository extends HashMap<String, File> {
	private static final long serialVersionUID = 413526382219042116L;
	private static SharedFileRepository sharedFileRepository = null;

	private SharedFileRepository() {
	}

	public static SharedFileRepository getsharedFileRepository() {
		if (sharedFileRepository == null) {
			synchronized (SharedFileRepository.class) {
				if (sharedFileRepository == null) {
					sharedFileRepository = new SharedFileRepository();
				}
			}
		}
		return sharedFileRepository;
	}

	public void printRepository() {
		Set<String> keys = SharedFileRepository.getsharedFileRepository().keySet();
		StringBuilder str = new StringBuilder();
		for (String filename : keys) {
			str.append("[" + filename + "]");
		}
		System.out.println(str.toString());
		System.out.println("------------------------------------");
	}
}