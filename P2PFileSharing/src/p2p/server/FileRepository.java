package p2p.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class FileRepository extends HashMap<String, Set<ServerThread>> {
	private static final long serialVersionUID = 6224297974858049264L;
	private static FileRepository fileRepository = null;

	private FileRepository() {
	}

	public static FileRepository getFileRepository() {
		if (fileRepository == null) {
			synchronized (FileRepository.class) {
				if (fileRepository == null) {
					fileRepository = new FileRepository();
				}
			}
		}
		return fileRepository;
	}

	public boolean containsFile(String filename) {
		return FileRepository.getFileRepository().containsKey(filename);
	}

	@Override
	public boolean containsKey(Object key) {
		// TODO Auto-generated method stub
		return super.containsKey((String) key);
	}

	@Override
	public boolean containsValue(Object st) {
		// TODO Auto-generated method stub
		boolean flag = false;
		Set<String> key = FileRepository.getFileRepository().keySet();
		Set<ServerThread> value = null;
		for (String filename : key) {
			if (FileRepository.getFileRepository().containsKey(filename)) {
				value = FileRepository.getFileRepository().get(filename);
				if (value.contains(st)) {
					flag = true;
					break;
				}
			}
		}
		return flag;
	}

	public void addByClient(ServerThread st) {
		Set<ServerThread> value = null;
		for (String filename : st.getFileSet()) {
			if (FileRepository.getFileRepository().containsKey(filename)) {
				value = FileRepository.getFileRepository().get(filename);
				value.add(st);
				FileRepository.getFileRepository().replace(filename, value);
			} else {
				value = new HashSet<ServerThread>();
				value.add(st);
				FileRepository.getFileRepository().put(filename, value);
			}
		}
	}

	public void removeByClient(ServerThread st) {
		Set<String> keys = FileRepository.getFileRepository().keySet();
		Set<ServerThread> value = null;
		List<String> tempKeysToBeRemove = new ArrayList<String>();

		for (String filename : keys) {
			value = FileRepository.getFileRepository().get(filename);
			Iterator<ServerThread> it = value.iterator();
			while (it.hasNext()) {
				if (it.next() == st) {
					it.remove();
				}
			}

			if (value.size() <= 0) {
				tempKeysToBeRemove.add(filename);
			}
		}

		for (String filename : tempKeysToBeRemove) {
			FileRepository.getFileRepository().remove(filename);
		}
//		printRepository();
	}

	public void shwoRepository() {
		Set<String> keys = FileRepository.getFileRepository().keySet();
		Set<ServerThread> value = null;
		StringBuilder str = new StringBuilder();
		for (String filename : keys) {
			str.append("[" + filename);
			value = FileRepository.getFileRepository().get(filename);
			for (ServerThread st : value) {
				str.append(ServerConstants.REPOSITORY_FIELD_SEPARATOR + st.getClientPath());
			}
			str.append("]");
		}
		// System.out.println(str.toString());
	}

	public void printRepository() {
		Set<String> keys = FileRepository.getFileRepository().keySet();
		Set<ServerThread> value = null;
		for (String filename : keys) {
			System.out.print(filename);
			value = FileRepository.getFileRepository().get(filename);
			for (ServerThread st : value) {
				System.out.print(ServerConstants.REPOSITORY_FIELD_SEPARATOR + st.getClientPath());
			}
			System.out.println();
		}
		System.out.println("------------------------------------");
	}

	public String searchByFilename(String filename) {
		StringBuilder matchedFiles = new StringBuilder();
		Set<ServerThread> value = null;
		if (FileRepository.getFileRepository().containsKey(filename)) {
			matchedFiles.append(filename);
			value = FileRepository.getFileRepository().get(filename);
			for (ServerThread st : value) {
				matchedFiles.append(ServerConstants.REPOSITORY_FIELD_SEPARATOR + st.getClientPath());
			}
		}
		return matchedFiles.toString();
	}

	public String searchByFilenameWithoutSearcher(String filename, ServerThread st) {
		Set<String> set = st.getFileSet();
		StringBuilder matchedFiles = new StringBuilder();
		if (set.contains(filename)) {
			return "";
		} else {
			Set<ServerThread> value = null;
			if (FileRepository.getFileRepository().containsKey(filename)) {
				matchedFiles.append(filename);
				value = FileRepository.getFileRepository().get(filename);
				for (ServerThread s : value) {
					matchedFiles.append(ServerConstants.REPOSITORY_FIELD_SEPARATOR + s.getClientDownloadPath());
				}
			}
		}
		return matchedFiles.toString();
	}
}
