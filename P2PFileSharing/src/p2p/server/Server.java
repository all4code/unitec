package p2p.server;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;


public class Server extends JFrame {
	
	private static final long serialVersionUID = -3880071714896401490L;
	
	private ServerSocket server = null;
	private Socket client = null;
	private ArrayList<ServerThread> clientList = null;
	boolean stop;
	private JTextPane msgHistory;
	private JScrollPane jsp;

	public Server() {
		stop = false;
		clientList = new ArrayList<ServerThread>();
		try {
			server = new ServerSocket(ServerConstants.SERVER_PORT);
			
			this.setSize(600,400);
			this.setTitle("P2P File Sharing System");
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			
			msgHistory = new JTextPane();
			msgHistory.setEditable(false);
			
			jsp = new JScrollPane(msgHistory);
			jsp.setPreferredSize(new Dimension(600,400));
			
			this.add(jsp,BorderLayout.CENTER);
			this.setVisible(true);
			
			
			appendNormlMessage("Server start");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void start() {
		try {
			while (!stop) {
				if (stop) {
					close();
				}
				client = server.accept();
				ServerThread st = new ServerThread(client, this, clientList);
				st.start();
				clientList.add(st);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	protected void appendWarnMessage(String str) {
		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setForeground(attrset, Color.red);
		StyleConstants.setFontSize(attrset, 16);
		insert(str, attrset);
	}

	protected void appendNormlMessage(String str) {
		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setFontSize(attrset, 16);
		insert(str, attrset);
	}
	
	public void insert(String str, AttributeSet attrset) {
		Document docs = msgHistory.getDocument();
		
		str = str + "\n";
		
		try {
			
			docs.insertString(docs.getLength(), str, attrset);
			msgHistory.setCaretPosition(msgHistory.getStyledDocument().getLength());
		
		} catch (BadLocationException ble) {
			System.out.println("BadLocationException:" + ble);
		}
	}
	
	private void close() {
		stop = true;
		try {
			if (server != null) {
				server.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Server().start();
	}

}
