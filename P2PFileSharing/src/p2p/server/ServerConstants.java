package p2p.server;

public interface ServerConstants {
	public static final int SERVER_PORT = 8888;

	/*
	 * CLIENT_JOIN: The centralised server will listen to a ServerSocket on a
	 * well known port. A new client will contact the registry at this port and
	 * connect to the File Sharing Server.
	 */
	public static final int CLIENT_JOIN = 1;

	/*
	 * CLIENT_PUBLISH: After registration with the Server, the client will send
	 * a list of files available to the client to the server. The server will
	 * store a copy of this list of files within its internal database
	 * structure. All files published will be indexed under the IP address of
	 * the remote client publishing the file list.
	 */
	public static final int CLIENT_PUBLISH = 2;

	/*
	 * CLIENT_SEARCH: A client looking for a file will contact the server. The
	 * server will look for the file in its database (exact match) and will
	 * return the IP address(es) of the remote client(s) hosting the specified
	 * file if an entry was found.
	 */
	public static final int CLIENT_SEARCH = 3;

	/*
	 * CLIENT_LEAVE: A well behaved client will send a CLIENT_LEAVE message to
	 * the server before leaving the network. This message will remove any
	 * content published by the leaving client.
	 */
	public static final int CLIENT_LEAVE = 4;

	/*
	 * CLIENT_PING: A response from a client indicating that the client is still
	 * connected to the system.
	 */
	public static final int CLIENT_PING = 5;

	/*
	 * SERVER_PING messages: The server will periodically send ping messages to
	 * connected client’s. If the client is connected to the server and is
	 * available the client will respond with a CLIENT_PING message
	 */
	public static final int SERVER_PING = 6;

	/*
	 * REMOTE_FETCH: A remote client will send a message to another client,
	 * requesting to download a file. If the file is not available the remote
	 * client will respond with an error message REMOTE_FETCH_ERROR stating that
	 * the file is not available. This might occur when the central server’s
	 * database is out of date.
	 */
	public static final int REMOTE_FETCH = 7;
	
	/*
	 * if the file is not available the remote client will respond with an error
	 * message
	 */
	public static final int REMOTE_FETCH_ERROR = 8;


	public static final String SERVER_IP_ADDRESS = "localhost";
	public static final String REPOSITORY_FIELD_SEPARATOR = "@=@";
	public static final String REPOSITORY_ITEM_SEPARATOR = "&=&";
	
	public static final long PING_INTERVAL = 10000;
	
	public static final int REMOTE_FETCH_SUCCESS = 9;
	public static final int DOWNLOAD_START = 10;
	public static final int REQUIRE_RANGE = 11;
}
