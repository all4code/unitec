package p2p.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class ServerThread extends Thread {
	private Socket client;
	private List<ServerThread> clientList;
	// maintain a shared file list of clients
	private Set<String> fileSet;
	private DataInputStream in;
	private DataOutputStream out;
	private boolean stop;
	private String clientIPAddress;
	private int clientPort, clientDownloadPort;
	private Server server;

	public ServerThread(Socket client, Server server, List<ServerThread> clientList) {
		this.client = client;
		this.server = server;
		this.clientList = clientList;
		this.clientIPAddress = client.getInetAddress().getHostAddress();
		this.clientPort = client.getPort();
		this.stop = false;
		this.fileSet = new HashSet<String>();
		try {
			in = new DataInputStream(client.getInputStream());
			out = new DataOutputStream(client.getOutputStream());

			// ping clients, if client disconnect, the ping task will cancel.
			// 10 seconds period
			Timer timer = new Timer();
			timer.schedule(new TimerTask(){

				@Override
				public void run() {
					try {
						out.writeInt(ServerConstants.SERVER_PING);
						out.flush();
					} catch (SocketException e) {
						server.appendNormlMessage((getClientInfo() + "client could be offline, the ping stop!"));
						timer.cancel();
						close();
					} catch (IOException e) {
						System.out.println(e.getMessage());
						timer.cancel();
						close();
					}
					
				}
				
			},ServerConstants.PING_INTERVAL,ServerConstants.PING_INTERVAL);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		String data = null;
		while (!stop) {
			if (stop) {
				close();
			}
			try {
				int msgType = in.readInt();
				server.appendWarnMessage("msgType: " + msgType + "\n");
				switch (msgType) {
				case ServerConstants.CLIENT_JOIN:
					server.appendWarnMessage("CLIENT_JOIN");
					
					//obtain the client download port
					data = in.readUTF();
					
					if(data != null && !"".equals(data.trim())){
						setClientDownloadPort(Integer.valueOf(data));
					}
					
					server.appendNormlMessage(getClientInfo() + "download port is: " + getClientDownloadPort());
					
					//send client ip and port back
					getOutputStream().writeUTF(getClientPath());
					break;
				case ServerConstants.CLIENT_LEAVE:
					server.appendWarnMessage("CLIENT_LEAVE");
					if(clientList.contains(this)){
						clientList.remove(this);
					}
					broadcastClientLeave();
					close();
					break;
				case ServerConstants.CLIENT_PING:
					server.appendWarnMessage("CLIENT_PING");
					data = in.readUTF();
					if (data != null) {
						server.appendNormlMessage(getClientInfo() + data);
					}
					break;
				case ServerConstants.CLIENT_PUBLISH:
					server.appendWarnMessage("CLIENT_PUBLISH");
					data = in.readUTF();
					
					//update file repository to include this client
					updateFileRepository(data);
					break;
				case ServerConstants.CLIENT_SEARCH:
					server.appendWarnMessage("CLIENT_SEARCH");
					data = in.readUTF();
					server.appendNormlMessage("ServerThread-112: search keyWords:  " + data);
					
					//to provide client search result
					provideSearchResult(data.trim().toLowerCase());
					break;
				}
			} catch (SocketException e) {
				// e.printStackTrace();
				server.appendWarnMessage(getClientInfo() + "left accidentally!\n");
				close();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
		close();
		server.appendWarnMessage(getClientInfo() + "Thread Exit\n");
	}

	private void broadcastClientLeave() {
		try {
			if (clientList != null && clientList.size() > 0) {
				for (ServerThread client : clientList) {
					client.getOutputStream().writeInt(ServerConstants.CLIENT_LEAVE);
					client.getOutputStream().writeUTF(this.getClientDownloadPath());
					client.getOutputStream().flush();
				}
			}
		} catch (SocketException e) {
			server.appendWarnMessage(getClientInfo() + "left accidentally!\n");
			close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void provideSearchResult(String data) {
		try {
			getOutputStream().writeInt(ServerConstants.CLIENT_SEARCH);
			if (data == null || "".equals(data)) {
				getOutputStream().writeUTF("");
			} else {
				getOutputStream().writeUTF(matchedFiles(data));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//construct a customised protocol to send search result
	private String matchedFiles(String data) {
		
		StringBuilder matchedFiles = new StringBuilder();
		Set<String> keys = FileRepository.getFileRepository().keySet();
		
		for (String key : keys) {
			if (key.toLowerCase().contains(data)) {
				
				// get matched file list exclude the searcher shared
				String str = FileRepository.getFileRepository().searchByFilenameWithoutSearcher(key, this);
				
				if (str.trim().equals("") || str == null) {
					continue;
				} else {
					matchedFiles.append(str).append(ServerConstants.REPOSITORY_ITEM_SEPARATOR);
				}
			}
		}
		return matchedFiles.toString();
	}

	private void updateFileRepository(String data) {
		String[] fileArray = stringToArray(data, ",");
		if (fileArray != null) {
			setFileSet(fileArray);

			if (isExist()) {
				FileRepository.getFileRepository().removeByClient(this);
			}
			FileRepository.getFileRepository().addByClient(this);
		} else {
			FileRepository.getFileRepository().removeByClient(this);
		}
	}

	private String[] stringToArray(String string, String split) {
		String[] array = null;
		if (string != null && !"".equals(string.trim())) {
			StringBuilder builder = new StringBuilder(string.trim());

			if (builder.indexOf(split) == 0) {
				builder.deleteCharAt(0);
			}
			if (builder.lastIndexOf(split) == (builder.length() - 1)) {
				builder.deleteCharAt(builder.length() - 1);
			}

			array = builder.toString().split(split);
		}
		return array;
	}

	private boolean isExist() {
		return FileRepository.getFileRepository().containsValue(this);
	}

	public void setFileSet(String[] filenames) {
		fileSet.clear();
		for (String filename : filenames) {
			fileSet.add(filename);
		}
	}

	public String getClientPath() {
		return getClientIPAddress() + ":" + getClientPort();
	}
	
	public String getClientDownloadPath() {
		return getClientIPAddress() + ":" + getClientDownloadPort();
	}

	private DataOutputStream getOutputStream() {
		return out;
	}

	private void close() {
		stop = true;
		if (clientList.contains(this)) {
			clientList.remove(this);
		}
		if (FileRepository.getFileRepository().containsValue(this)) {
			FileRepository.getFileRepository().removeByClient(this);
		}
		try {
			if (in != null) {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (client != null) {
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String getClientInfo(){
		return "[" + getClientIPAddress() + ":" + getClientPort() + ":" + getClientDownloadPort() + "] ";
		
	}
	public String getClientIPAddress() {
		return clientIPAddress;
	}

	public void setClientIPAddress(String clientIPAddress) {
		this.clientIPAddress = clientIPAddress;
	}

	public int getClientDownloadPort() {
		return clientDownloadPort;
	}

	public int getClientPort() {
		return clientPort;
	}

	public void setClientPort(int clientPort) {
		this.clientPort = clientPort;
	}

	public void setClientDownloadPort(int clientDownloadPort) {
		this.clientDownloadPort = clientDownloadPort;
	}

	public Set<String> getFileSet() {
		return fileSet;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (String s : fileSet) {
			str.append(s + ",");
		}
		return "[" + clientIPAddress + ":" + clientDownloadPort + "] " + str;
	}
}
