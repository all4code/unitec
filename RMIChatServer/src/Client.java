import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JOptionPane;

public class Client {

	private String nickname;
	ClientCallbackInterface clientCallback;
	ServerInterface chat;

	public static void main(String args[]) {
		new Client();
	}

	public Client() {
		if (System.getSecurityManager() == null) {

			System.setProperty("java.security.policy", "wideopen.policy");
			System.setSecurityManager(new SecurityManager());
		}
		try {
			Registry registry = LocateRegistry.getRegistry("localhost", ServerConstants.RMI_PORT);

			chat = (ServerInterface) registry.lookup(ServerConstants.name);

			boolean isNotValidNickname = true;

			do {
				nickname = JOptionPane.showInputDialog(null, "Please input your nickname", "Login", JOptionPane.PLAIN_MESSAGE);
				if (nickname == null) {
					exit();
				} else if ("".equals(nickname.trim())) {
					JOptionPane.showMessageDialog(null, "You must enter a valid nickname", "Warning", JOptionPane.ERROR_MESSAGE);
				} else if (chat.isExistNickname(nickname)) {
					JOptionPane.showMessageDialog(null, "The nickname is exist!", "Warning", JOptionPane.WARNING_MESSAGE);
				} else {
					isNotValidNickname = false;
				}

			} while (isNotValidNickname);
			
			clientCallback = new ClientCallbackImpl(nickname, chat);

			// register callback for client
			chat.registerClientCallback(nickname, clientCallback);
			chat.sendToAll("Server", nickname + " connected!");



		} catch (RemoteException e) {
			System.err.println("BookServer exception:");
			exit();
//			e.printStackTrace();
		} catch (NotBoundException e) {
//			e.printStackTrace();
			System.out.println(e.getMessage());
			exit();
		} 
	}

	public void exit() {
		System.out.println("Client exit!");
		try {
			if(clientCallback != null){
				chat.unregisterClientCallback(clientCallback);
			}
			if (nickname != null && !"".equals(nickname.trim())) {
				chat.sendToAll("Server", nickname + " logging off!");
			}
			System.exit(0);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
