import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public class ClientCallbackImpl extends UnicastRemoteObject implements ClientCallbackInterface, ActionListener {
	private static final long serialVersionUID = -786208686227223575L;
	// define the user interface components
	JFrame clientFrame = new JFrame();
	JTextField chatInput = new JTextField(50);
//	JTextArea chatHistory = new JTextArea(5, 50);
	JTextPane chatHistory = new JTextPane();
	JButton chatMessage = new JButton("Send");

	// define the user login dialog
	private String nickname;
	private JPanel bottomPanel;
	private JList<String> clientList;
	private DefaultListModel<String> clientModel;
	private JScrollPane jspClientList;
	private ServerInterface chat;

	protected ClientCallbackImpl() throws RemoteException {
		super();

	}

	public ClientCallbackImpl(String nickname, ServerInterface chat) throws RemoteException {
		super();
		this.nickname = nickname;
		this.chat = chat;
		initFrame();
	}

	private void initFrame() {
		// TODO Auto-generated method stub
		clientFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		clientFrame.setSize(500, 400);
		// create the user interface and setup an action listener linked to
		// the send message button

		Container contentPane = clientFrame.getContentPane();
		contentPane.setLayout(new BorderLayout());

		clientFrame.setTitle(nickname);

		// TODO add in extra user interface components to allow a user to
		// select the remote client that they want to send a message to
		// suggest using a JList
		clientModel = new DefaultListModel<String>();
		clientList = new JList<String>(clientModel);
		clientList.setFixedCellWidth(100);
		jspClientList = new JScrollPane(clientList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		contentPane.add(jspClientList, BorderLayout.WEST);

		bottomPanel = new JPanel();
		bottomPanel.add(chatInput);
		bottomPanel.add(chatMessage);

		contentPane.add(bottomPanel, BorderLayout.SOUTH);

		chatHistory.setEditable(false);
		contentPane.add(new JScrollPane(chatHistory), BorderLayout.CENTER);

		clientFrame.pack();
		clientFrame.setVisible(true);

		clientFrame.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent event) {
				exit();
			}
		});

		chatMessage.addActionListener(this);
		chatInput.addActionListener(this);
	}

	@Override
	public void serverCallback(String msg) throws RemoteException {
		System.err.println(msg);
//		this.chatHistory.append(msg + "\n");
		if(msg.endsWith(ServerConstants.PRIVATE_MSG_SUFFIX)){
			setPrivateMsg(msg);
		}else if(msg.endsWith(ServerConstants.PUBLIC_MSG_SUFFIX)){
			setPublicMsg(msg);
		}else{
			setBroadcastMsg(msg);
		}
		chatHistory.setCaretPosition(chatHistory.getStyledDocument().getLength());
	}
	
	private void setBroadcastMsg(String str) {
		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setForeground(attrset, Color.darkGray);
//		StyleConstants.setItalic(attrset, true);
		StyleConstants.setFontSize(attrset, 16);
		insert(str, attrset);
	}
	
	private void setPrivateMsg(String str) {
		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setForeground(attrset, Color.red);
//		StyleConstants.setItalic(attrset, true);
		StyleConstants.setBold(attrset, true);
		StyleConstants.setFontSize(attrset, 20);
		insert(str, attrset);
	}

	private void setPublicMsg(String str) {
		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setForeground(attrset, Color.blue);
//		StyleConstants.setUnderline(attrset, true);
//		StyleConstants.setItalic(attrset, true);
		StyleConstants.setFontSize(attrset, 18);
		insert(str, attrset);
	}
	
	public void insert(String str, AttributeSet attrset) {
		Document docs = chatHistory.getDocument();
		
		str = str + "\n";
		try {
			docs.insertString(docs.getLength(), str, attrset);
		} catch (BadLocationException ble) {
			System.out.println("BadLocationException:" + ble);
		}
	}

	@Override
	public void setNickname(String nickname) throws RemoteException {
		this.nickname = nickname;
	}

	@Override
	public String getNickname() throws RemoteException {
		return nickname;
	}

	@Override
	public void updateUserList(String clients) throws RemoteException {
		String[] clientusersArray = clients.split(",");

		clientModel.clear();
		for (String u : clientusersArray) {
			if (!(u.trim()).equalsIgnoreCase(nickname.trim())) {
				clientModel.addElement(u);
			}
		}
		clientModel.addElement("Everyone");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			String chatMsg = chatInput.getText();

			if (chatMsg == null || chatMsg.trim().equals("")) {

				JOptionPane.showMessageDialog(clientFrame, "No message can be found!", "Warning", JOptionPane.WARNING_MESSAGE);

			} else if (chatMsg.trim().equals(ServerConstants.CLIENT_EXIT_MSG)) {

				int action = JOptionPane.showConfirmDialog(clientFrame, "Do you want to exit?");

				if (action == JOptionPane.YES_OPTION) {
					exit();
				}

			} else {

				List<String> selectedUser = clientList.getSelectedValuesList();

				boolean isAll = false;
				String strSelectedUser = "";

				if (selectedUser.size() <= 0) {
					isAll = true;
				} else {
					for (String user : selectedUser) {
						if (user.trim().equals("Everyone")) {
							isAll = true;
							break;
						} else {
							strSelectedUser += user + ",";
						}
					}
				}
				if (isAll) {
					chat.sendToOthers(nickname, chatInput.getText());
				} else {
					chat.sendPrivateMsg(nickname, strSelectedUser, chatInput.getText());
				}
			}

			chatInput.setText("");
		} catch (HeadlessException e1) {
			e1.printStackTrace();
			exit();
		} catch (RemoteException e1) {
			e1.printStackTrace();
			exit();
		}
	}
	
	public void exit() {
		System.out.println("ClientImpl exit!");
		try {
			chat.unregisterClientCallback(this);
			if (nickname != null && !"".equals(nickname.trim())) {
				chat.sendToAll("Server", nickname + " logging off!");
			}
			System.exit(0);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
}
