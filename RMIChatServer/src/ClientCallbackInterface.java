
import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ClientCallbackInterface extends Remote {
	// parameters need to be Serializable
	public void serverCallback(String msg) throws RemoteException;
	public String getNickname() throws RemoteException;
	public void setNickname(String nickname) throws RemoteException;
	public void updateUserList(String clients) throws RemoteException;
}
