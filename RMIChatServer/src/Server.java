import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class Server implements ServerInterface {
	ArrayList<ClientCallbackInterface> clients = new ArrayList<ClientCallbackInterface>();

	public static void main(String[] args) {
		if (System.getSecurityManager() == null) {
			SecurityManager securityManager = new SecurityManager();

			System.setProperty("java.security.policy", "rmi.policy");

			System.setSecurityManager(securityManager);
		}
		try {
			ServerInterface proxy = new Server();
			ServerInterface stub = (ServerInterface) UnicastRemoteObject.exportObject(proxy, 0);

			// create a new RMI Registry and bind to user port defined in the ServerConstants
			LocateRegistry.createRegistry(ServerConstants.RMI_PORT);

			// get a reference to the registry
			Registry registry = LocateRegistry.getRegistry(ServerConstants.RMI_PORT);

			registry.rebind(ServerConstants.name, stub);
			System.out.println(ServerConstants.name + " bound");
		} catch (Exception e) {
			System.err.println(ServerConstants.name + " exception:");
			e.printStackTrace();
		}
	}

	@Override
	public void sendToAll(String nickname, String msg) throws RemoteException {
		System.err.println(msg);
		for (ClientCallbackInterface client : clients) {
			client.serverCallback("[" + nickname + "]: " + msg);
		}
	}

	@Override
	public void sendToOthers(String nickname, String msg) throws RemoteException {
		for (ClientCallbackInterface client : clients) {
			if (!(client.getNickname().trim()).equalsIgnoreCase(nickname.trim())) {
				client.serverCallback("[" + nickname + "]: " + msg + ServerConstants.PUBLIC_MSG_SUFFIX);
			}
		}
	}

	@Override
	public void sendPrivateMsg(String nickname, String strClients, String msg) throws RemoteException {
		// TODO Auto-generated method stub
		String[] clientArray = strClients.split(",");
		for (ClientCallbackInterface client : clients) {
			if (isTargetClient(client.getNickname(), clientArray)) {
				client.serverCallback("[" + nickname + "]: " + msg + ServerConstants.PRIVATE_MSG_SUFFIX);
			}
		}
	}

	@Override
	public void registerClientCallback(String nickname, ClientCallbackInterface clientCallback) throws RemoteException {
		clientCallback.setNickname(nickname);
		clients.add(clientCallback);
		updateClientList();
		System.err.println(clientCallback.getNickname() + " connected! ");
	}

	@Override
	public void unregisterClientCallback(ClientCallbackInterface clientCallback) throws RemoteException {
		// System.err.println("Client" + clientCallback.toString() +
		// " logging off.");
		clients.remove(clientCallback);
		updateClientList();
		System.err.println("Client: " + clientCallback.getNickname() + " logging off.");

	}

	@Override
	public boolean isExistNickname(String nickname) throws RemoteException {
		for (ClientCallbackInterface client : clients) {
			if (client.getNickname().equalsIgnoreCase(nickname)) {
				return true;
			}
		}
		return false;
	}

	private boolean isTargetClient(String nickname, String[] clientArray) {
		// TODO Auto-generated method stub
		for (String clientNickname : clientArray) {
			if (clientNickname.equalsIgnoreCase(nickname)) {
				return true;
			}
		}
		return false;
	}

	private void updateClientList() throws RemoteException {
		for (ClientCallbackInterface client : clients) {
			client.updateUserList(getClientsStr(clients));
			System.out.println(client.getNickname());
		}
	}

	private String getClientsStr(ArrayList<ClientCallbackInterface> clients) throws RemoteException {
		String strClients = "";

		for (ClientCallbackInterface client : clients) {
			strClients += client.getNickname() + ",";
		}
		return strClients;
	}
}
