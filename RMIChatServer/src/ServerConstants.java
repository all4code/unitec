
public class ServerConstants {
	public static final String name = "ChatServer";
	public static final String CLIENT_EXIT_MSG = "bye";
	public static final String PRIVATE_MSG_SUFFIX = " [Private]";
	public static final String PUBLIC_MSG_SUFFIX = " [Public]";
	
	public static final int RMI_PORT = 60000;
}
