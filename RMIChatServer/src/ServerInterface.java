
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {
	public void sendToAll(String nickname, String msg) throws RemoteException;
	public void sendToOthers(String nickname, String msg) throws RemoteException;
	public void sendPrivateMsg(String nickname, String strClients, String msg) throws RemoteException;
	public void unregisterClientCallback(ClientCallbackInterface clientCallback) throws RemoteException;
	public boolean isExistNickname(String nickname) throws RemoteException;
	public void registerClientCallback(String nickname, ClientCallbackInterface clientCallback) throws RemoteException;
}
