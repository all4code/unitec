import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

public class Client extends JFrame implements ActionListener, Runnable {

	private static final long serialVersionUID = 980389841528802556L;

	// define the user interface components
	JTextField chatInput = new JTextField(50);
	JTextArea chatHistory = new JTextArea(5, 50);
	JButton chatMessage = new JButton("Send");

	// define the socket and io streams
	Socket client;
	DataInputStream dis;
	DataOutputStream dos;

	// define the user login dialog
	private String nickname;
	private JPanel bottomPanel;
	private JList<String> clientList;
	private DefaultListModel<String> clientModel;
	private JScrollPane jspClientList;
	private boolean runThread = true;
	private Runnable run;

	public Client() {

		// attempt to connect to the defined remote host
		try {
			client = new Socket("localhost", 5000);
			dis = new DataInputStream(client.getInputStream());
			dos = new DataOutputStream(client.getOutputStream());

			// TODO develop prompt to allow a user (client) to enter the
			// nickname / handle for their client suggest using JOptionPane
			boolean isNotValidNickname = true;

			do {
				nickname = JOptionPane.showInputDialog(this, "Please input your nickname", "Login", JOptionPane.PLAIN_MESSAGE);
				if (nickname == null) {
					dos.writeInt(ServerConstants.EXIT_MESSAGE_BEFORE_LOGIN);
					close();
					System.exit(NORMAL);
				} else if ("".equals(nickname.trim())) {
					JOptionPane.showMessageDialog(this, "You must enter a valid nickname", "Warning", JOptionPane.ERROR_MESSAGE);
				} else if (isExistNickName(nickname)) {
					JOptionPane.showMessageDialog(this, "The nickname is exist!", "Warning", JOptionPane.WARNING_MESSAGE);
				} else {
					isNotValidNickname = false;
				}

			} while (isNotValidNickname);

			// // define a thread to take care of messages sent from the server

			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			this.setSize(500, 400);
			// create the user interface and setup an action listener linked to
			// the send message button

			Container contentPane = this.getContentPane();
			contentPane.setLayout(new BorderLayout());
			
			this.setTitle(nickname);

			// TODO add in extra user interface components to allow a user to
			// select the remote client that they want to send a message to
			// suggest using a JList
			clientModel = new DefaultListModel<String>();
			clientList = new JList<String>(clientModel);
			clientList.setFixedCellWidth(100);
			jspClientList = new JScrollPane(clientList, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			contentPane.add(jspClientList, BorderLayout.WEST);

			bottomPanel = new JPanel();
			bottomPanel.add(chatInput);
			bottomPanel.add(chatMessage);

			contentPane.add(bottomPanel, BorderLayout.SOUTH);

			contentPane.add(new JScrollPane(chatHistory), BorderLayout.CENTER);

			pack();
			setVisible(true);

			this.addWindowListener(new WindowAdapter() {

				@Override
				public void windowClosing(WindowEvent event) {
					// TODO Auto-generated method stub
					try {
						dos.writeInt(ServerConstants.EXIT_MESSAGE);
						dos.writeUTF(nickname);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						close();
					}
				}
			});

			chatMessage.addActionListener(this);
			chatInput.addActionListener(this);

			// define a thread to take care of messages sent from the server
			Thread clientThread = new Thread(this);
			clientThread.start();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean isExistNickName(String inputNickname) {
		// TODO Auto-generated method stub
		// send REGISTER message
		try {
			dos.writeInt(ServerConstants.REGISTER_CLIENT); // determine the
			dos.writeUTF(inputNickname); // message payload
			dos.flush();
			
			//if it is not a existName, return "ok", otherwise "REGISTER_USER_EXIST"
			String strExistNickname = dis.readUTF();

			if (strExistNickname.equalsIgnoreCase(ServerConstants.REGISTER_USER_EXIST)) {
				return true;
			} else {
				return false;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return true;
		}
	}

	/**
	 * Method to respond to button press events, and send a chat message to the
	 * Server
	 */

	@Override
	public void actionPerformed(ActionEvent event) {
		try {
			String chatMsg = chatInput.getText();

			if (chatMsg == null || chatMsg.trim().equals("")) {

				JOptionPane.showMessageDialog(this, "No message can be found!", "Warning", JOptionPane.WARNING_MESSAGE);

			} else if (chatMsg.trim().equals(ServerConstants.CLIENT_EXIT_MSG)) {
				
				int action = JOptionPane.showConfirmDialog(this, "Do you want to exit?");

				if (action == JOptionPane.YES_OPTION) {
					dos.writeInt(ServerConstants.EXIT_MESSAGE);
					dos.writeUTF(nickname);
					close();
					System.exit(NORMAL);
				}
				
			} else {

				List<String> selectedUser = clientList.getSelectedValuesList();

				boolean isAll = false;
				String strSelectedUser = "";

				if (selectedUser.size() <= 0) {
					isAll = true;
				} else {
					for (String user : selectedUser) {
						if (user.trim().equals("Everyone")) {
							isAll = true;
							break;
						} else {
							strSelectedUser += user + ",";
						}
					}
				}
				if (isAll) {
					dos.writeInt(ServerConstants.CHAT_MESSAGE);
					dos.writeUTF(chatInput.getText());
				} else {
					dos.writeInt(ServerConstants.PRIVATE_MESSAGE);
					dos.writeUTF(ServerConstants.PRIVATE_USER_LIST_START + strSelectedUser + ServerConstants.PRIVATE_USER_LIST_END + chatInput.getText());
				}
			}
			dos.flush(); // force the message to be sent (sometimes data can
							// be buffered)

			chatInput.setText("");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void close() {
		runThread = false;
		// TODO Auto-generated method stub
		try {
			if (dis != null)
				dis.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			if (dos != null)
				dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (client != null)
				client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// process messages from the server
	@Override
	public void run() {
		while (runThread) {
			try {
				int messageType = dis.readInt(); // receive a message from the
													// server, determine message
													// type based on an integer

				// decode message and process
				switch (messageType) {
				case ServerConstants.CHAT_BROADCAST:
					chatHistory.append(dis.readUTF());
					break;

				case ServerConstants.UPDATE_USERLIST:
					upcateClientList(dis.readUTF());
					SwingUtilities.invokeLater(run);
					break;
				case ServerConstants.REGISTER_BROADCAST:
					chatHistory.append(dis.readUTF());
					break;
				}
			} catch (IOException e) {
				// e.printStackTrace();
			}
		}
	}

	private void upcateClientList(String clientList) {
		String[] clientusersArray = clientList.split(",");
		//This can solve the problem of which the content of JList is occasionally no display 
		run = new Runnable(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				clientModel.clear();
				for (String u : clientusersArray) {
					if (!(u.substring(0, u.indexOf("["))).equalsIgnoreCase(nickname)) {
						clientModel.addElement(u);
					}
				}
				clientModel.addElement("Everyone");
			}
		};
		
	}

	public static void main(String[] args) {
		new Client();
	}
}
