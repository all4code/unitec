package OthersWork;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;


public class Client extends JFrame implements ActionListener, Runnable, ItemListener{
	
	private static final long serialVersionUID = 980389841528802556L;
	

	boolean mExitThread = false;
	// define the user interface components
	JTextField chatInput = new JTextField(30);
	JTextArea chatHistory = new JTextArea(5,50);
	JButton chatMessage = new JButton("Send");
	JList clientList = new JList();
	JCheckBox isPublic;
	
	DefaultListModel<String> clientListModal;
	String mLoginName;
	
	// define the socket and io streams
	Socket client;
	DataInputStream dis;
	DataOutputStream dos;
	

	private boolean IsLoginNameValid(String loginName)
	{
		
		try
		{
			dos.writeInt(ServerConstants.REGISTER_CLIENT);
			dos.writeUTF(loginName);
			dos.flush();

			int messageType = dis.readInt();
			assert messageType==ServerConstants.REGISTER_RESPONSE;
			int response = dis.readInt();
			if(1 == response)
				return true;
			else
				return false;
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	public Client()
	{
		// create the user interface and setup an action listener linked to the send message button
		
		Container contentPane = this.getContentPane();
		
		try
		{
			client = new Socket("localhost",5000);
			dis = new DataInputStream(client.getInputStream());
			dos = new DataOutputStream(client.getOutputStream());
			
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(0);
		}
		catch(SocketException e)
		{
			e.printStackTrace();
			System.exit(0);
		}
		catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		String loginName = null;
		Boolean loginNameValid = false;
		do
		{
			loginName = (String)JOptionPane.showInputDialog(this, "Please input a new login name:", 
				"Login", JOptionPane.PLAIN_MESSAGE, null, null, "");
		
			if(0==loginName.length())
			{
				JOptionPane.showMessageDialog(this, "Please Input a valid user name!");
			}
			else if(loginName != null)
			{
				loginNameValid = IsLoginNameValid(loginName);
				if(!loginNameValid)
				{
					JOptionPane.showMessageDialog(this, "User name exists, please input another user name!");
				}
			}
			else
			{
				System.exit(0);
			}
		}while(loginNameValid == false);
			
		this.setTitle(loginName);
		
		mLoginName = loginName;
		clientListModal = new DefaultListModel<String>();
		//PrintListModal("after new DefaultListModal");
		clientList.setModel(clientListModal);
		clientList.setLayoutOrientation(JList.VERTICAL);
		clientList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		/*
		contentPane.setLayout(new BorderLayout());
		contentPane.add(chatInput,BorderLayout.CENTER);
		contentPane.add(chatMessage,BorderLayout.EAST);
		contentPane.add(new JScrollPane(chatHistory),BorderLayout.NORTH);
		contentPane.add(new JScrollPane(clientList), BorderLayout.WEST);*/
		
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		JPanel upperPane = new JPanel();
		JPanel bottomPane = new JPanel();
		contentPane.add(upperPane);
		contentPane.add(bottomPane);
		//BoxLayout boxLayout = new BoxLayout(contentPane, BoxLayout.Y_AXIS);
		
		upperPane.setLayout(new BoxLayout(upperPane, BoxLayout.X_AXIS));
		bottomPane.setLayout(new BoxLayout(bottomPane, BoxLayout.X_AXIS));
		
		//upperPane.add(new JScrollPane(chatHistory));
		//upperPane.add(new JScrollPane(clientList));
		
		JPanel upperRightPane = new JPanel();
		JPanel upperLeftPane = new JPanel();
		upperPane.add(upperLeftPane);
		upperPane.add(upperRightPane);
		
		upperLeftPane.setLayout(new BoxLayout(upperLeftPane, BoxLayout.X_AXIS));
		upperRightPane.setLayout(new BoxLayout(upperRightPane, BoxLayout.Y_AXIS));
		
		isPublic = new JCheckBox("send message to every one");
		upperLeftPane.add(chatHistory);
		JScrollPane scrollClientList = new JScrollPane(clientList);
		
		upperRightPane.add(scrollClientList);
		upperRightPane.add(isPublic);
		isPublic.setAlignmentX(Component.LEFT_ALIGNMENT);
		scrollClientList.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		bottomPane.add(chatInput);
		bottomPane.add(chatMessage);
		clientList.setAlignmentY(TOP_ALIGNMENT);
		
		pack();
		setVisible(true);
		
		
		isPublic.addItemListener(this);
		isPublic.setSelected(true);
		chatMessage.addActionListener(this);
		
		// attempt to connect to the defined remote host
			// define a thread to take care of messages sent from the server
		Thread clientThread = new Thread(this);
		clientThread.start();

		
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				try
				{
					dos.writeInt(ServerConstants.EXIT_MESSAGE);
					dos.flush();
				}
				catch (IOException ex){
					ex.printStackTrace();
				}
				mExitThread = true;
			}
		});
	}
	
	public void itemStateChanged(ItemEvent e)
	{
		Object obj = e.getItem();
		if(obj.equals(isPublic))
		{
			if(isPublic.isSelected())
				clientList.setEnabled(false);
			else
				clientList.setEnabled(true);
		}
	}

	/**
	 * Method to respond to button press events, and send a chat message to the Server
	 */
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		try {
			
			
			if(isPublic.isSelected())
			{
				dos.writeInt(ServerConstants.CHAT_MESSAGE); // determine the type of message to be sent
			}
			else
			{
				String selVal = (String)clientList.getSelectedValue();
				if(selVal==null || selVal.length()==0)
				{
					JOptionPane.showMessageDialog(this, "Please select a user to send a message!");
				}
				else
				{
					//send private message
					dos.writeInt(ServerConstants.PRIVATE_MESSAGE);
					dos.writeUTF(selVal);				
				}
			}
			dos.writeUTF(chatInput.getText()); // message payload
			dos.flush(); // force the message to be sent (sometimes data can be buffered)
		}
		catch (IOException e){
			e.printStackTrace();
		}
	}

	public void PrintListModal(String prefix)
	{
		System.err.println(prefix+"\n");
		for(int i=0; i<clientListModal.size(); ++i)
		{
			System.err.println(clientListModal.elementAt(i)+",");
		}
		System.err.println("\n");
	}
	// process messages from the server
	@Override
	public void run()
	{
		while(!mExitThread)
		{
			try {
				int messageType = dis.readInt(); // receive a message from the server, determine message type based on an integer
				DefaultListModel<String> listModal = clientListModal;
				//PrintListModal("in run function");
				//System.err.println("messageType:"+messageType+"\n");
				// decode message and process
				switch(messageType)
				{
					case ServerConstants.CHAT_BROADCAST:
						chatHistory.append(dis.readUTF()+"\n");
						break;
					case ServerConstants.PRIVATE_MESSAGE:
						String sender = dis.readUTF();
						chatHistory.append(sender + ":" + dis.readUTF() + "\n");
						break;
					case ServerConstants.REGISTER_BROADCAST:
						int count = dis.readInt();
						for(int i=0; i<count; ++i)
						{
							String clientName = dis.readUTF();
							if(!mLoginName.equals(clientName))	//do not display myself
							{
								if(-1 == listModal.indexOf(clientName))
								{
									//System.err.println("addElement:"+clientName+"\n");
									listModal.addElement(clientName);
								}
							}
						}
						break;
					case ServerConstants.EXIT_MESSAGE:
						mExitThread = true;
						break;
					case ServerConstants.EXIT_BROADCAST:
						String exitName = dis.readUTF();
						listModal.removeElement(exitName);
						break;

				}
			}
			catch (SocketException e)
			{
				if(!mExitThread)
					e.printStackTrace();
			}
			catch (IOException e)
			{
				if(!mExitThread)
					e.printStackTrace();
			}
		}
		try
		{
			dos.close();
			dis.close();
			client.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		this.dispose();
	}
	public static void main(String[] args)
	{
		Client client = new Client();
		client.setVisible(true);
	}
}
