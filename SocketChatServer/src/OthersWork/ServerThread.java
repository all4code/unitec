package OthersWork;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;


public class ServerThread extends Thread {

	DataInputStream dis;
	DataOutputStream dos;
	
	Socket remoteClient;
	Server server;
	String clientName;
	boolean mExitThread = false;
	ArrayList<ServerThread> connectedClients; // keep track of all the other clients connected to the Server
	
	public ServerThread(Socket remoteClient, Server server, ArrayList<ServerThread> connectedClients)
	{
		this.remoteClient = remoteClient;
		this.connectedClients = connectedClients;
		try {
			this.dis = new DataInputStream(remoteClient.getInputStream());
			this.dos = new DataOutputStream(remoteClient.getOutputStream());
			this.server = server;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public void close()
	{
		mExitThread = true;
		try
		{
			dos.writeInt(ServerConstants.EXIT_MESSAGE);
			dos.writeUTF(this.clientName);
			dos.flush();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return;
		}
	}

	public void run()
	{
		while(!mExitThread) // main protocol decode loop
		{
			try {
				int mesgType = dis.readInt(); // read the type of message from the client (must be an integer)
				System.err.println(mesgType);
				
				// decode the message type based on the integer sent from the client
				switch(mesgType)
				{
					case ServerConstants.CHAT_MESSAGE:
						String data = dis.readUTF();
						System.err.println(data);
						server.getSystemLog().append(remoteClient.getInetAddress()+"(" +clientName+"):"+
						remoteClient.getPort()+">"+data+"\n");
						
						for(ServerThread otherClient: connectedClients)
						{
							if(!otherClient.equals(this)) // don't send the message to the client that sent the message in the first place
							{
								otherClient.getDos().writeInt(ServerConstants.CHAT_BROADCAST);
								otherClient.getDos().writeUTF(data);
								otherClient.getDos().flush();
							}
						}
						
						break;
					case ServerConstants.REGISTER_CLIENT:
						String regClientName = dis.readUTF();
						//System.err.println(regClientName);
						server.getSystemLog().append(remoteClient.getInetAddress()+":Register--"+regClientName+"\n");
						clientName = regClientName;
						
						getDos().writeInt(ServerConstants.REGISTER_RESPONSE);
						boolean existLoginName = false;
						for(ServerThread c:connectedClients)
						{
							if(!c.equals(this))
							{
								if(c.clientName.equals(regClientName))
								{
									existLoginName = true;
									break;
								}
							}
						}
						if(existLoginName)
						{
							getDos().writeInt(0);
							mExitThread = true;
							return;
						}
						else
							getDos().writeInt(1);

						// broadcast this registration to all other clients connected to the server (similar to the CHAT_BROADCAST message sent to each client above)
						for(ServerThread client:connectedClients)
						{
							client.getDos().writeInt(ServerConstants.REGISTER_BROADCAST);
							client.getDos().writeInt(connectedClients.size());
							for(ServerThread c:connectedClients)
							{
								client.getDos().writeUTF(c.clientName);
							}
							client.getDos().flush();
						}
						break;
					case ServerConstants.EXIT_MESSAGE:
						server.getSystemLog().append(remoteClient.getInetAddress()+":Exit--"+this.clientName+"\n");
						
						connectedClients.remove((Object)this);

						for(ServerThread client:connectedClients)
						{
							client.getDos().writeInt(ServerConstants.EXIT_BROADCAST);
							client.getDos().writeUTF(this.clientName);
							client.getDos().flush();
						}
						mExitThread = true;
						break;
					case ServerConstants.PRIVATE_MESSAGE:
						String destClient = dis.readUTF();
						String msg = dis.readUTF();
						for(ServerThread client:connectedClients)
						{
							if(client.clientName.equals(destClient))
							{
								client.getDos().writeInt(ServerConstants.PRIVATE_MESSAGE);
								client.getDos().writeUTF(this.clientName);
								client.getDos().writeUTF(msg);
								client.getDos().flush();
							}
						}
						break;
				}				
			}
			catch (IOException e)
			{
				if(!mExitThread)
				{
					e.printStackTrace();
					//if remote client close, close the thread
					close();
				}
			}
		}
		
		try
		{
			dis.close();
			dos.close();
			remoteClient.close();
		}
		catch(SocketException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		System.err.println("Thread Exist\n");
	}

	public DataOutputStream getDos() {
		return dos;
	}
}
