package OthersWork;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

public class Test {
	
	static Runnable doHelloWorld;
	
	public static void main(String[] args) {
		doThing();
		SwingUtilities.invokeLater(doHelloWorld);
//		try {
//			SwingUtilities.invokeAndWait(doHelloWorld);
//		} catch (InvocationTargetException | InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		for(int i = 0; i<10;i++){
			 System.out.println("This might well be displayed before the other message." + i);
		}
	}
	
	public static void doThing(){
		doHelloWorld = new Runnable() {
		     public void run() {
		    	 for (int i = 0; i < 10; i++) {
		    		 System.out.println("Hello World on " + Thread.currentThread());
				}
		     }
		 };
	}
}
