public class ServerConstants {
	public static final int CHAT_MESSAGE = 0;
	public static final int EXIT_MESSAGE = 1;
	public static final int CHAT_BROADCAST = 2;
	public static final int PRIVATE_MESSAGE = 3; //TODO develop your own protocol to handle private messages
	public static final int REGISTER_CLIENT = 4; // TODO develop your own protocol to handle the registration of a new client. This message will need to send a client's nickname to the centralised server.
	public static final int REGISTER_BROADCAST = 5; // TODO develop your own protocol to handle the broadcast of a new client, to all other clients connected to the server
	
	// for updating the user list in client window
	public static final int UPDATE_USERLIST = 6; 
	public static final int EXIT_MESSAGE_BEFORE_LOGIN = 7;
	
	/*
	 *  for dividing the private users and message
	 *  ths message format is:
	 *  PRIVATE_USER_LIST_STARTuser1[xxx.xxx.xxx.xxx],user2[xxx.xxx.xxx.xxx],PRIVATE_USER_LIST_ENDmessage
	 */
	public static final String PRIVATE_USER_LIST_START = "PRIVATE_USER_LIST_START";
	public static final String PRIVATE_USER_LIST_END = "PRIVATE_USER_LIST_END";
	public static final String REGISTER_USER_EXIST = "REGISTER_USER_EXIST";
	public static final String CLIENT_EXIT_MSG = "exit";
}
