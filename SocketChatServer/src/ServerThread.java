import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ServerThread extends Thread {

	DataInputStream dis;
	DataOutputStream dos;

	Socket remoteClient;
	Server server;

	ArrayList<ServerThread> connectedClients; // keep track of all the other
												// clients connected to the
												// Server
	static Map<String, ServerThread> clientListMap;
	String nickname;

	private boolean stop = false;

	public ServerThread(Socket remoteClient, Server server, ArrayList<ServerThread> connectedClients) {
		this.remoteClient = remoteClient;
		this.connectedClients = connectedClients;
		// for maintain client list
		if (clientListMap == null) {
			clientListMap = new HashMap<String, ServerThread>();
		}
		try {
			this.dis = new DataInputStream(remoteClient.getInputStream());
			this.dos = new DataOutputStream(remoteClient.getOutputStream());
			this.server = server;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		String data = null;

		// while (true) // main protocol decode loop
		while (!stop) {
			try {
				int mesgType = dis.readInt(); // read the type of message from
												// the client (must be an
												// integer)
				System.err.println(mesgType);

				// decode the message type based on the integer sent from the
				// client
				switch (mesgType) {
				case ServerConstants.CHAT_MESSAGE:
					data = dis.readUTF();
					System.err.println(data);
					server.getSystemLog().append(nickname + "[" + remoteClient.getInetAddress().getHostAddress() + ":" + remoteClient.getPort() + "]" + data + "\n");

					for (ServerThread otherClient : connectedClients) {
						// don't send the message to the client that sent the
						// message in the first place
						if (!otherClient.equals(this)) {
							otherClient.getDos().writeInt(ServerConstants.CHAT_BROADCAST);
							otherClient.getDos().writeUTF(nickname + "[" + remoteClient.getInetAddress() + ":" + remoteClient.getPort() + "]" + data + " [Public]\n");
						}
					}
					break;
				case ServerConstants.REGISTER_CLIENT:
					
					// TODO develop code to handle new client registrations
					data = dis.readUTF();
					System.err.println(data);
					
					if (isExistNickname(data)) {
						
						getDos().writeUTF(ServerConstants.REGISTER_USER_EXIST);
						
					} else {
						
						getDos().writeUTF("ok");
						nickname = data;
						server.getSystemLog().append("<" + remoteClient.getInetAddress().getHostAddress() + ":" + remoteClient.getPort() + "> " + data + " is connected !\n");
						clientListMap.put(data + "[" + remoteClient.getInetAddress().getHostAddress() + "]", this);

						// TODO broadcast this registration to all other clients connected to the server (similar to the CHAT_BROADCAST message sent to each client above)
						for (ServerThread otherClient : connectedClients) {
							otherClient.getDos().writeInt(ServerConstants.UPDATE_USERLIST);
							Set<String> set = clientListMap.keySet();
							String clients = "";
							for (String str : set) {
								clients += str + ",";
							}
							otherClient.getDos().writeUTF(clients);
							otherClient.getDos().writeInt(ServerConstants.REGISTER_BROADCAST);
							otherClient.getDos().writeUTF(data + " is connected !  Typing \"exit\" to exit! \n");
						}
					}
					break;
				case ServerConstants.PRIVATE_MESSAGE:
					// TODO develop code to handle private messages sent by the
					// client
					data = dis.readUTF();
					System.err.println(data);

					String[] usersArray = getPrivateUsers(data);
					String msg = getPrivateMsg(data);

					for (String u : usersArray) {
						clientListMap.get(u).getDos().writeInt(ServerConstants.CHAT_BROADCAST);
						clientListMap.get(u).getDos().writeUTF(nickname + "<" + remoteClient.getInetAddress() + ":" + remoteClient.getPort() + ">" + msg + " [Private] \n");
					}

					break;
				case ServerConstants.EXIT_MESSAGE:// client exit normally
					data = dis.readUTF();
					System.err.println(data);
					nickname = data;
					connectedClients.remove(this);
					server.connectedClients.remove(this);
					clientListMap.remove(data + "[" + remoteClient.getInetAddress().getHostAddress() + "]", this);

					server.getSystemLog().append("<" + remoteClient.getInetAddress().getHostAddress() + ":" + remoteClient.getPort() + "> " + data + " is left ! \n");

					for (ServerThread otherClient : connectedClients) {
						otherClient.getDos().writeInt(ServerConstants.UPDATE_USERLIST);
						Set<String> set = clientListMap.keySet();
						String clients = "";
						for (String str : set) {
							clients += str + ",";
						}
						otherClient.getDos().writeUTF(clients);
						otherClient.getDos().writeInt(ServerConstants.CHAT_BROADCAST);
						otherClient.getDos().writeUTF(data + " is left ! \n");
					}
					close();
					break;

				case ServerConstants.EXIT_MESSAGE_BEFORE_LOGIN:// login has been
																// cancelled
					System.err.println("login cancelled");
					connectedClients.remove(this);
					server.connectedClients.remove(this);
					close();
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
	}

	private boolean isExistNickname(String inputNickname) {
		boolean returnFlag = false;
		// TODO Auto-generated method stub
		System.err.println("clientListMap: " + clientListMap.size());
		if (clientListMap == null || clientListMap.size() <= 0) {
			returnFlag = false;
		} else {
			Set<String> clients = clientListMap.keySet();
			for (String u : clients) {
				System.err.println("nicknames: " + u);
				if (inputNickname.equalsIgnoreCase(u.substring(0,u.indexOf("[")))) {
					returnFlag = true;
					break;
				}
			}
		}
		System.err.println("isExitNickname = " + returnFlag);
		return returnFlag;
	}

	private void close() {
		stop = true;
		try {
			if (dis != null)
				dis.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if (dos != null)
				dos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if(remoteClient != null){
				remoteClient.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("socket on serverThread is closed: " + this.nickname);
	}

	private String[] getPrivateUsers(String data) {
		int start = data.indexOf(ServerConstants.PRIVATE_USER_LIST_START);
		int length = ServerConstants.PRIVATE_USER_LIST_START.length();
		int end = data.indexOf(ServerConstants.PRIVATE_USER_LIST_END);
		String users = data.substring(start + length, end);
		return users.split(",");
	}

	private String getPrivateMsg(String data) {
		int start = data.indexOf(ServerConstants.PRIVATE_USER_LIST_END);
		int length = ServerConstants.PRIVATE_USER_LIST_END.length();
		return data.substring(start + length);
	}

	public DataOutputStream getDos() {
		return dos;
	}
}
